<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

	//
	//protected $fillable = ['brandid','userid'];
	public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 'm_statuses';

	public static function filter($filter = [])
	{
		$query = Status::orderBy('description');

		if(isset($filter['enabled'])) {
			$query->where('enabled', $filter['enabled']);
		}
		// $entities = $query->get();

		return $query->get();
	}

}
