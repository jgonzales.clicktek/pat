<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessUnit extends Model {

	//
	public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 'm_business_units';

	public static function filter($filter = [])
	{
		$query = BusinessUnit::orderBy('description');

		if(isset($filter['enabled'])) {
			$query = $query->where('enabled', $filter['enabled']);
		}

		// $units = $query->get();

		return $query->get();
	}	

}
