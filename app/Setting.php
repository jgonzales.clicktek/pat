<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	public $timestamps = false;
	protected $table = 'm_settings';

	public static function filter($filter = [])
	{
		$query = Setting::orderBy('description');

		if(isset($filter['enabled'])) {
			$query->where('enabled', $filter['enabled']);
		}
		// $entities = $query->get();

		return $query->get();
	}

}
