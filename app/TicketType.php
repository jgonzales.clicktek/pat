<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model {

	public $timestamps = false;
	protected $table = 'm_ticket_types';

}
