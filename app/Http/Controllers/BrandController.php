<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Brand;
use App\Entity;
use Auth;
use DB;

class BrandController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$entities = Entity::all();

		$brands = Brand::filterBrands();

		$options = ['No', 'Yes'];

		$brands = Brand::filterBrands();

		return view('brand.list', compact('brands', 'filter', 'entities', 'options'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$entities = Entity::all();
		return view('brand.add')->with('entities', $entities);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// $request->replace(array('description' => trim($request->description)));
		
		$this->validate($request, [
			'entity' => 'required',
			'description' => 'required|unique:m_brands,description,NULL,id,entity_id,'.$request->entity
	    ]);

		$brand = new Brand;
		$brand->entity_id = $request->entity;
		$brand->description = $request->description;
		$brand->enabled = 1;
		$brand->save();

		return redirect('/brand');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$brand = Brand::findOrFail($id);
		$entities = Entity::all();

		return view('brand.edit')
				->with('entities', $entities)
				->with('brand', $brand);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		
	    $this->validate($request, [
	    	'entity' => 'required',
	        'description' => 'required|unique:m_brands,description,'.$id.',id,entity_id,'.$request->entity
	    ]);

	    $brand = Brand::findOrFail($id);
	    $brand->entity_id = $request->entity;
	    $brand->description = $request->description;
	    $brand->enabled = $request->has('enabled');
	    $brand->save();

	    return redirect('/brand');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Brand::destroy($id);
		return redirect('/brand')->with('success', 'Entity successfully deleted!');
	}

	public function filterBrand(Request $request)
	{
		$entity = '';
		$filter = array();
		$options = ['No', 'Yes'];

		$entities = Entity::all();

		if ($request->has('entity')) {
			$filter['entity'] = $request->entity;
		}

		if ($request->has('enabled')) {
			$filter['enabled'] = $request->enabled;
		}

		$brands = Brand::filterBrands($filter);

		return view('brand.list', compact('brands', 'filter', 'entities', 'options'));
	}

	public function getBrands(Request $request)
	{

		// $brands = Brand::getBrandByEntity($request->entity);
		$brands = Brand::where('enabled', 1)->orderBy('description')->get();

		return $brands->toJson(JSON_PRETTY_PRINT);
	}

}
