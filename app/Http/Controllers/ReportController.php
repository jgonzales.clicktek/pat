<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Resource;
use App\Entity;
use App\Brand;
use App\Project;
use App\Activity;
use App\Status;
use App\Setting;
use App\BusinessUnit;
use Auth;
use DB;
use Exception;

//use App\report;
use config;

class ReportController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the entity sharing report for a particular resource.
     *
     * @param Request $request
     * @return Response
     */

    public function index(Request $request)
    {
        $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();

        $entities = Entity::where('enabled', 1)->orderBy('description')->get();
        $brands = [];

        $resouceId = ($request->has('resource')) ? $request->resource : NULL;

        $filter = [
            'from' => date('Y-m-d'),
            'to' => date('Y-m-d'),
            'resource' => $resouceId,
            'entity' => '',
            'brand' => '-1'
        ];

        if ($request->has('fromDate')) {
            $filter['from'] = $request->fromDate;
            $filter['to'] = $request->toDate;
        }

        DB::enableQueryLog();

        $pmQuery = DB::table('t_activities')
            ->select('t_activities.id', 'hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                DB::raw('(t_projects.description) as project'),
                DB::raw('(m_entities.description) as entity'),
                DB::raw('(m_brands.description) as brand'),
                DB::raw('(m_statuses.description) as status'),
                DB::raw('m_ticket_types.name AS ticket_type'))
            ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
            ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
            ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
            ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
            ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
            ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
            ->whereBetween('date_log', [$filter['from'], $filter['to']])
            ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');

        $activities = $pmQuery->distinct()->paginate(20);

        if ($request->submit == "download") {
            // 	dd(DB::getQueryLog());
            // die();
            // echo "test"; die;

            $this->makeCSV($activities);

            return response()->json(array('body' => $data), 200);
            // exit;
            //} else {
        }
        return view('report.myteam', compact('resources', 'filter', 'activities', 'entities', 'brands'))->render();
        //}
    }

    public function showSharing(Request $request)
    {
        $lastId = '';
        $name = '';
        $defaultResource = '';
        $selectedResource = -1;
        $includeAdmin = 1;
        $summary = [];

        $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
        $entities = Entity::where('enabled', 1)->orderBy('description')->get();

        $fromDate = $toDate = date('Y-m-d');

        if ($request->has('fromDate')) {
            $fromDate = $request->fromDate;
            $toDate = $request->toDate;
        }

        DB::enableQueryLog();

        $activities = Activity::select('resource_id', 'lastname', 'firstname',
            DB::raw('(m_entities.description) as entity'),
            DB::raw('SUM(hours) as total_hours'))
            // ->join('t_projects', 't_projects.id', '=', 't_activities.project_id')
            ->join('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
            ->join('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
            ->where('t_activities.hours', '>', '0')
            ->whereBetween('date_log', [$fromDate, $toDate])
            ->groupBy(DB::raw('resource_id,lastname,firstname,entity'))
            ->orderByRaw(DB::raw('firstname,lastname'));

        if ($request->has('resource')) {
            $activities->where('t_activities.resource_id', $request->resource);
            $defaultResource = $request->resource;
        }

        if (!$request->has('includeAdmin')) {
            $activities->where('m_entities.billable', 1)->where('t_activities.billable', 1);
            $includeAdmin = 0;
        }

        if ($request->has('resource_type') && $request->resource_type >= 0) {
            $activities->where('m_resources.is_internal', $request->resource_type);
        }

        $result = $activities->get();

        // dd(DB::getQueryLog());
        // die;

        $resourceTypes = ['Outsource', 'Internal'];
        $selectedResource = $request->resource_type;

        try {

            $settings = Setting::find(1);

            $summary = $this->setSummary($result);

            // if ($settings->entity_sharing_old) {
            // 	$this->addPercentageToSummary($summary);
            // } else {
            // 	$this->newPercentageSummary($summary);
            // }
            $this->PercentageCalculation($summary);

        } catch (Exception $e) {
            return view('report.sharing-old', compact('summary', 'resources', 'entities', 'defaultResource', 'includeAdmin', 'fromDate', 'toDate', 'resourceTypes', 'selectedResource'));
        }

        // if ($settings->entity_sharing_old) {
        return view('report.sharing-old', compact('summary', 'resources', 'entities', 'defaultResource', 'includeAdmin', 'fromDate', 'toDate', 'resourceTypes', 'selectedResource'));
        // } else {
        // 	return view('report.sharing', compact('summary', 'resources', 'entities', 'defaultResource', 'includeAdmin', 'fromDate', 'toDate', 'resourceTypes', 'selectedResource'));
        // }

    }

    private function setSummary($result)
    {
        $summary = [];
        $lastId = '';
        $totalHours = 0;


        if (!$result->isEmpty()) {
            foreach ($result as $rst) {
                if ($rst->resource_id != $lastId) {
                    if ($lastId !== '') {        //meaning: another resource/not new resource
                        $summary[$lastId]['total_hours'] = $totalHours;
                    }

                    $name = $rst->firstname . " " . $rst->lastname;
                    $totalHours = 0;
                }
                $summary[$rst->resource_id]['name'] = $name;
                $summary[$rst->resource_id]['company'][$rst->entity]['hours'] = $rst->total_hours;

                $totalHours += $rst->total_hours;
                $lastId = $rst->resource_id;
            }
            $summary[$lastId]['total_hours'] = $totalHours;
        }

        return $summary;
    }

    // private function addPercentageToSummary(&$summary)
    // {
    // 	// echo "<pre>";
    // 	// print_r($summary);
    // 	// die;

    // 	try {

    // 		foreach($summary as $key => $val) {
    // 			$companyPercentage = [];
    // 			$totalPercentage = 0;

    // 			$totalHours = $summary[$key]['total_hours'];

    // 			foreach($val['company'] as $name => $value) {
    // 				$hours = $summary[$key]['company'][ $name ]['hours'];
    // 				$percentage = round($hours / $totalHours * 100, 4);
    // 				$summary[$key]['company'][ $name ]['percent'] = $percentage;
    // 				$totalPercentage += $percentage;
    // 				$companyPercentage[ $name ] = $percentage;
    // 			}

    // 			if($totalPercentage != 100) {
    // 				$diff = (100 - $totalPercentage);
    // 				$additionalPercentage = round($diff / count($companyPercentage), 4);

    // 				$totalPercentage = 0;
    // 				$i = count($companyPercentage);
    // 				asort($companyPercentage);
    // 				foreach($companyPercentage as $name => $value) {
    // 					--$i;
    // 					$summary[$key]['company'][ $name ]['percent'] += $additionalPercentage;
    // 					$totalPercentage += $summary[$key]['company'][ $name ]['percent'];
    // 					if($i == 0 && $totalPercentage !== 100) { //last company
    // 						$summary[$key]['company'][ $name ]['percent'] += (100 - $totalPercentage);
    // 					}
    // 				}
    // 			}
    // 		}

    // 	} catch(Exception $e) {
    // 		throw new Exception("Error found. ".$e->getMessage(), 1);

    // 	}

    // }

    // private function newPercentageSummary(&$summary)
    // {

    // 	try {

    // 		foreach($summary as $key => $val) {
    // 			$companyPercentage = [];
    // 			$totalPercentage = 0;

    // 			$totalHours = $summary[$key]['total_hours'];

    // 			foreach($val['company'] as $name => $value) {
    // 				$hours = $summary[$key]['company'][ $name ]['hours'];
    // 				$percentage = $hours / $totalHours * 100;
    // 				$summary[$key]['company'][ $name ]['percent'] = $percentage;
    // 				$totalPercentage += $percentage;
    // 				$companyPercentage[ $name ] = $percentage;
    // 			}
    // 		}

    // 	} catch(Exception $e) {
    // 		throw new Exception("Error found. ".$e->getMessage(), 1);

    // 	}

    // }

    private function PercentageCalculation(&$summary)
    {

        try {
            foreach ($summary as $key => $val) {
                $companyPercentage = [];
                $totalPercentage = 0;

                $totalHours = $summary[$key]['total_hours'];

                foreach ($val['company'] as $name => $value) {
                    $hours = $summary[$key]['company'][$name]['hours'];
                    //$percentage = ($hours * 100) / $totalHours;
                    $percentage = round($hours / $totalHours * 100, 4);
                    $summary[$key]['company'][$name]['percent'] = $percentage;
                    //$totalPercentage += $percentage;
                    //$companyPercentage[ $name ] = $percentage;
                }
            }
        } catch (Exception $e) {
            throw new Exception("Error found. " . $e->getMessage(), 1);
        }
    }

    public function showTeamActivity(Request $request)
    {
        //$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
        $resources = DB::table('t_projects')
            ->select('t_projects.project_manager_id', 'm_resources.id', 'm_resources.firstname', 'm_resources.lastname')
            ->leftjoin('m_resources', 't_projects.project_manager_id', '=', 'm_resources.id')
            ->whereNotNull('t_projects.project_manager_id')
            ->where('enabled', 1)
            ->groupby('t_projects.project_manager_id')
            ->orderBy('m_resources.firstname')
            ->get();
        $entities = Entity::where('enabled', 1)->orderBy('description')->get();
        $brands = [];
        $resouceId = ($request->has('resource')) ? $request->resource : NULL;

        $filter = [
            'from' => date('Y-m-d'),
            'to' => date('Y-m-d'),
            'resource' => $resouceId,
            'entity' => '',
            'brand' => '-1'
        ];

        if ($request->has('fromDate')) {
            $filter['from'] = $request->fromDate;
            $filter['to'] = $request->toDate;
        }

        DB::enableQueryLog();


        $pmQuery = DB::table('t_activities')
            ->select('t_activities.id', 'hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                DB::raw('(t_projects.description) as project'),
                DB::raw('(m_entities.description) as entity'),
                DB::raw('(m_brands.description) as brand'),
                DB::raw('(m_statuses.description) as status'),
                DB::raw('m_ticket_types.name AS ticket_type'))
            ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
            ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
            ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
            ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
            ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
            ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
            ->whereBetween('date_log', [$filter['from'], $filter['to']])
            //->whereBetween('date_log', [$request->fromDate, $request->toDate])
            ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');

        //if ($request->has('resource')) {
        //$filter['resource'] = $resouceId;
        //$pmQuery = $pmQuery->where('project_manager_id', $filter['resource']);
        //}
        if (trim($request->resource) != '') {
            $pmQuery->where('project_manager_id', $request->resource);
        }

        //if ($request->has('brand')) {
        //$filter['brand'] = $request->brand;
        //$filter['entity'] = $request->entity;
        //$brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
        //$pmQuery = $pmQuery->where('t_activities.brand_id', $filter['brand']);
        //}
        if (trim($request->brand) != '') {
            $brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
            $pmQuery = $pmQuery->where('t_activities.brand_id', $request->brand);
        }

        //if ($request->has('from') && $request->has('to')) {
        //$pmQuery->whereBetween('date_log', [$filter['from'], $filter['to']]);
        //}
        if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
            $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
        }

        // $activities = $pmQuery->distinct()->paginate(40);

        // // 	dd(DB::getQueryLog());
        // // die();

        // if ($request->submit == "download")
        // {
        // 	// 	dd(DB::getQueryLog());
        // 	// die();
        // 		// echo "test"; die;
        // 	$this->makeCSV($activities);
        // 		// exit;
        // 	//} else {
        // }

        if ($request->submit == "download") {
            $activityReport = $pmQuery;
            $this->makeCSV($activityReport->distinct()->get());
        }
        $activities = $pmQuery->distinct()->paginate(20);

        return view('report.myteam', compact('resources', 'filter', 'activities', 'entities', 'brands'))->render();
        //}
    }

    public function fetchData(Request $request)
    {
        if ($request->ajax()) {
            DB::enableQueryLog();
            //$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
            $resources = DB::table('t_projects')
                ->select('t_projects.project_manager_id', 'm_resources.id', 'm_resources.firstname', 'm_resources.lastname')
                ->leftjoin('m_resources', 't_projects.project_manager_id', '=', 'm_resources.id')
                ->whereNotNull('t_projects.project_manager_id')
                ->where('enabled', 1)
                ->groupby('t_projects.project_manager_id')
                ->orderBy('m_resources.firstname')
                ->get();

            $entities = Entity::where('enabled', 1)->orderBy('description')->get();
            $brands = [];

            $resouceId = ($request->has('resource')) ? $request->resource : NULL;

            $filter = [
                'from' => date('Y-m-d'),
                'to' => date('Y-m-d'),
                'resource' => $resouceId,
                'entity' => '',
                'brand' => '-1'
            ];

            if ($request->has('fromDate')) {
                $filter['from'] = $request->fromDate;
                $filter['to'] = $request->toDate;
            }

            //DB::enableQueryLog();

            $pmQuery = DB::table('t_activities')
                ->select('t_activities.id', 'hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                    DB::raw('(t_projects.description) as project'),
                    DB::raw('(m_entities.description) as entity'),
                    DB::raw('(m_brands.description) as brand'),
                    DB::raw('(m_statuses.description) as status'),
                    DB::raw('m_ticket_types.name AS ticket_type'))
                ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
                ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                //->whereBetween('date_log', [$filter['from'], $filter['to']])
                //->whereBetween('date_log', [$request->fromDate, $request->toDate])
                ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');

            //if ($request->has('resource')) {
            //$filter['resource'] = $resouceId;
            //$pmQuery = $pmQuery->where('project_manager_id', $filter['resource']);
            //}
            if (trim($request->resource) != '') {
                $pmQuery->where('project_manager_id', $request->resource);
            }


            //if ($request->has('brand')) {
            //$filter['brand'] = $request->brand;
            //$filter['entity'] = $request->entity;
            //$brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
            //$pmQuery = $pmQuery->where('t_activities.brand_id', $filter['brand']);
            //}
            if (trim($request->brand) != '') {
                $brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
                $pmQuery = $pmQuery->where('t_activities.brand_id', $request->brand);
            }

            //if ($request->has('from') && $request->has('to')) {
            //$pmQuery->whereBetween('date_log', [$filter['from'], $filter['to']]);
            //}
            if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
                $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
            }

            // $activities = $pmQuery->distinct()->paginate(20);

            // // 	dd(DB::getQueryLog());
            // // die();

            // if ($request->submit == "download")
            // {
            // 	// 	dd(DB::getQueryLog());
            // 	// die();
            // 	// echo "test"; die;
            // 	$this->makeCSV($activities);

            // 	//$data = $this->makeCSV($activities);

            // 	//return response()->json(array('body'=>''), 200);

            // 	// exit;
            // 	//} else {
            // }


            if ($request->submit == "download") {
                $activityReport = $pmQuery;
                $this->makeCSV($activityReport->distinct()->get());
            }
            $activities = $pmQuery->distinct()->paginate(20);

            return view('report.data', compact('resources', 'filter', 'activities', 'entities', 'brands'))->render();
            //}
        }
    }

    private function makeCSV($activities)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Report_by_Ticket_Owner.csv"');

        $fp = fopen('php://output', 'wb');

        $header = ["Ticket Ref", "Ticket Name", "First Name", "Last Name", "Status", "Entity", "Brand", "Date Log", "Ticket Type", "Hours"];
        fputcsv($fp, $header);

        foreach ($activities as $activity) {

            $ticketType = ($activity->ticket_type == NULL) ? "" : $activity->ticket_type;
            $val = [
                $activity->project_ref,
                $activity->project,
                $activity->firstname,
                $activity->lastname,
                $activity->status,
                $activity->entity,
                $activity->brand,
                $activity->date_log,
                $ticketType,
                $activity->hours
            ];

            fputcsv($fp, $val);
        }
        fclose($fp);
        exit();
    }

    public function RptbyBusinessUnit(Request $request)
    {
        $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
        $entities = Entity::where('enabled', 1)->orderBy('description')->get();
        $businessunits = BusinessUnit::where('enabled', 1)->orderBy('description')->get();
        $brands = [];
        $resouceId = ($request->has('resource')) ? $request->resource : NULL;

        $filter = [
            'from' => date('Y-m-d'),
            'to' => date('Y-m-d'),
            'resource' => $resouceId,
            'entity' => '',
            'brand' => '-1',
            'businessunit' => '0'
        ];

        if ($request->has('fromDate')) {
            $filter['from'] = $request->fromDate;
            $filter['to'] = $request->toDate;
        }

        DB::enableQueryLog();
        if (trim($request->businessunit) == '0') {
            $pmQuery = DB::table('t_activities')
                ->select('t_activities.id', 'hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                    DB::raw('(t_projects.description) as project'),
                    DB::raw('(m_entities.description) as entity'),
                    DB::raw('(m_brands.description) as brand'),
                    DB::raw('(m_statuses.description) as status'),
                    DB::raw('m_ticket_types.name AS ticket_type'))
                ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
                ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                ->whereBetween('date_log', [$filter['from'], $filter['to']])
                ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');
        } else {
            $pmQuery = DB::table('t_activities')
                ->select('t_activities.id', 'hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                    DB::raw('(t_projects.description) as project'),
                    DB::raw('(m_entities.description) as entity'),
                    DB::raw('(m_brands.description) as brand'),
                    DB::raw('(m_statuses.description) as status'),
                    DB::raw('m_ticket_types.name AS ticket_type'))
                ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
                ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                ->where('m_resources.business_unit_id', $request->businessunit)
                ->whereBetween('date_log', [$filter['from'], $filter['to']])
                ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');
        }

        // if(trim($request->businessunit) != '')
        // {
        // 	//$businessunits = BusinessUnit::where('enabled', 1)->orderBy('description')->get();
        // 	$pmQuery = $pmQuery->where('m_resources.business_unit_id', $request->businessunit);
        // }

        // if (trim($request->businessunit) != '')
        // {
        // 	$resources = Resource::where('enabled', 1)->where('business_unit_id', $request->businessunit)->orderBy('firstname')->orderBy('lastname')->get();
        // 	$pmQuery->where('project_manager_id', $resources);
        // }

        if (trim($request->brand) != '') {
            $brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
            $pmQuery = $pmQuery->where('t_activities.brand_id', $request->brand);
        }

        if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
            $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
        }

        if ($request->submit == "download") {
            $activityReport = $pmQuery;
            $this->makeCSV_businessunit($activityReport->distinct()->get());
        }
        $activities = $pmQuery->distinct()->paginate(20);

        return view('report.reportbybusinessfilter', compact('resources', 'filter', 'activities', 'entities', 'brands', 'businessunits'))->render();
    }

    public function RptbyBusinessUnit_fetchdata(Request $request)
    {
        if ($request->ajax()) {
            $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
            $entities = Entity::where('enabled', 1)->orderBy('description')->get();
            $businessunits = BusinessUnit::where('enabled', 1)->orderBy('description')->get();
            $brands = [];

            $resouceId = ($request->has('resource')) ? $request->resource : NULL;

            $filter = [
                'from' => date('Y-m-d'),
                'to' => date('Y-m-d'),
                'resource' => $resouceId,
                'entity' => '',
                'brand' => '-1',
                'businessunit' => '0'
            ];

            if ($request->has('fromDate')) {
                $filter['from'] = $request->fromDate;
                $filter['to'] = $request->toDate;
            }

            DB::enableQueryLog();
            if (trim($request->businessunit) == '0') {
                $pmQuery = DB::table('t_activities')
                    ->select('t_activities.id', 'hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                        DB::raw('(t_projects.description) as project'),
                        DB::raw('(m_entities.description) as entity'),
                        DB::raw('(m_brands.description) as brand'),
                        DB::raw('(m_statuses.description) as status'),
                        DB::raw('m_ticket_types.name AS ticket_type'))
                    ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                    ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
                    ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                    ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                    ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                    ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                    ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');
            } else {
                $pmQuery = DB::table('t_activities')
                    ->select('t_activities.id', 'hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                        DB::raw('(t_projects.description) as project'),
                        DB::raw('(m_entities.description) as entity'),
                        DB::raw('(m_brands.description) as brand'),
                        DB::raw('(m_statuses.description) as status'),
                        DB::raw('m_ticket_types.name AS ticket_type'))
                    ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                    ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
                    ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                    ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                    ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                    ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                    ->where('m_resources.business_unit_id', $request->businessunit)
                    ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');
            }

            // if(trim($request->businessunit) != '')
            // {
            // 	//$businessunits = BusinessUnit::where('enabled', 1)->orderBy('description')->get();
            // 	$pmQuery = $pmQuery->where('m_resources.business_unit_id', $request->businessunit);
            // }

            // if (trim($request->resource) != '')
            // {
            // 	$resources = Resource::where('enabled', 1)->where('business_unit_id', $request->businessunit)->orderBy('firstname')->orderBy('lastname')->get();
            // 	$pmQuery->where('project_manager_id', $resources);
            // }

            if (trim($request->brand) != '') {
                $brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
                $pmQuery = $pmQuery->where('t_activities.brand_id', $request->brand);
            }

            if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
                $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
            }

            if ($request->submit == "download") {
                $activityReport = $pmQuery;
                $this->makeCSV_businessunit($activityReport->distinct()->get());
            }
            $activities = $pmQuery->distinct()->paginate(20);

            return view('report.reportbybusiness', compact('resources', 'filter', 'activities', 'entities', 'brands', 'businessunits'))->render();
        }
    }

    private function makeCSV_businessunit($activities)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Report_by_Business_Unit.csv"');

        $fp = fopen('php://output', 'wb');

        $header = ["Ticket Ref", "Ticket Name", "First Name", "Last Name", "Status", "Entity", "Brand", "Date Log", "Ticket Type", "Hours"];
        fputcsv($fp, $header);

        foreach ($activities as $activity) {

            $ticketType = ($activity->ticket_type == NULL) ? "" : $activity->ticket_type;
            $val = [
                $activity->project_ref,
                $activity->project,
                $activity->firstname,
                $activity->lastname,
                $activity->status,
                $activity->entity,
                $activity->brand,
                $activity->date_log,
                $ticketType,
                $activity->hours
            ];
            fputcsv($fp, $val);
        }
        fclose($fp);
        exit();
    }

    public function RptbyResAct(Request $request)
    {
        $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
        $entities = Entity::where('enabled', 1)->orderBy('description')->get();
        $brands = [];
        $resouceId = ($request->has('resource')) ? $request->resource : NULL;

        $filter = [
            'from' => date('Y-m-d'),
            'to' => date('Y-m-d'),
            'resource' => $resouceId,
            'entity' => '',
            'brand' => '-1'
        ];

        if ($request->has('fromDate')) {
            $filter['from'] = $request->fromDate;
            $filter['to'] = $request->toDate;
        }

        DB::enableQueryLog();
        $pmQuery = DB::table('t_activities')
            ->select('activity', 'date_log', 't_activities.id', 'hours', 'project_id', 'project_ref', 't_activities.billable',
                DB::raw('concat(m_resources.firstname, " ", m_resources.lastname) as resource'),
                DB::raw('(t_projects.description) as project'),
                DB::raw('(m_entities.description) as entity'),
                DB::raw('(m_brands.description) as brand'),
                DB::raw('(m_statuses.description) as status'),
                DB::raw('m_ticket_types.name AS ticket_type'),
                DB::raw('m_types.description AS task_type'))
            ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
            ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
            ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
            ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
            ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
            ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
            ->leftJoin('m_types', 'm_types.id', '=', 't_activities.type_id')
            ->whereBetween('date_log', [$filter['from'], $filter['to']])
            ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');

        if (trim($request->resource) != '') {
            $pmQuery->where('resource_id', $request->resource);
        }

        if (trim($request->brand) != '') {
            $brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
            $pmQuery = $pmQuery->where('t_activities.brand_id', $request->brand);
        }

        if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
            $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
        }

        if ($request->submit == "download") {
            $activityReport = $pmQuery;
            $this->makeCSV_resactivity($activityReport->distinct()->get());
        }

        $activities = $pmQuery->distinct()->paginate(20);


        return view('report.reportbyresourceactivityfilter', compact('resources', 'filter', 'activities', 'entities', 'brands'))->render();
    }

    public function RptbyResAct_fetchdata(Request $request)
    {
        if ($request->ajax()) {
            $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
            $entities = Entity::where('enabled', 1)->orderBy('description')->get();
            $brands = [];

            $resouceId = ($request->has('resource')) ? $request->resource : NULL;

            $filter = [
                'from' => date('Y-m-d'),
                'to' => date('Y-m-d'),
                'resource' => $resouceId,
                'entity' => '',
                'brand' => '-1'
            ];

            if ($request->has('fromDate')) {
                $filter['from'] = $request->fromDate;
                $filter['to'] = $request->toDate;
            }

            DB::enableQueryLog();
            $pmQuery = DB::table('t_activities')
                ->select('activity', 'date_log', 't_activities.id', 'hours', 'project_id', 'project_ref', 't_projects.project_manager_id',
                    DB::raw('concat(m_resources.firstname, " ", m_resources.lastname) as resource'),
                    DB::raw('concat(pm.firstname, " ", pm.lastname) as project_manager'),
                    DB::raw('(t_projects.description) as project'),
                    DB::raw('(m_entities.description) as entity'),
                    DB::raw('(m_brands.description) as brand'),
                    DB::raw('(m_statuses.description) as status'),
                    DB::raw('m_ticket_types.name AS ticket_type'),
                    DB::raw('m_types.description AS task_type'))
                ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
                ->leftJoin(DB::raw('m_resources as pm on pm.id = t_projects.project_manager_id'))
                ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                ->leftJoin('m_types', 'm_types.id', '=', 't_activities.type_id')
                ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC', 't_projects.project_manager_id');

            if (trim($request->resource) != '') {
                $pmQuery->where('resource_id', $request->resource);
            }

            if (trim($request->brand) != '') {
                $brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
                $pmQuery = $pmQuery->where('t_activities.brand_id', $request->brand);
            }

            if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
                $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
            }

            if ($request->submit == "download") {
                $activityReport = $pmQuery;
                $this->makeCSV_resactivity($activityReport->distinct()->get());
            }
            $activities = $pmQuery->distinct()->paginate(20);

            return view('report.reportbyresourceactivity', compact('resources', 'filter', 'activities', 'entities', 'brands'))->render();
        }
    }

    private function makeCSV_resactivity($activities)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Report_by_Resource_.csv"');

        $fp = fopen('php://output', 'wb');

        $header = ["Date Log", "Ticket Ref", "Ticket Name", "Resource", "Activity", "Ticket Type", "Task Type", "Entity", "Brand", "Status", "Billable", "Hours"];
        fputcsv($fp, $header);

        foreach ($activities as $activity) {

            $ticketType = ($activity->ticket_type == NULL) ? "" : $activity->ticket_type;
            $Billble = ($activity->billable == 1) ? "Yes" : "No";
            $val = [
                $activity->date_log,
                $activity->project_ref,
                $activity->project,
                $activity->resource,
                $activity->activity,
                $ticketType,
                $activity->task_type,
                $activity->entity,
                $activity->brand,
                $activity->status,
                $Billble,
                $activity->hours
            ];
            fputcsv($fp, $val);
        }
        fclose($fp);
        exit();
    }

    public function RptprojActLog(Request $request)
    {
        //dd($request);
        //$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
        $units = DB::table('m_business_units')
            ->select('id', 'description')
            ->where('enabled', '1')
            ->groupby('id', 'description')
            ->orderby('description', 'asc')
            ->get();


        foreach ($units as $unitKey => $unit) {
            $resource = DB::table('m_resources')
                ->select('id', DB::raw('CONCAT(firstname," ", lastname) as name'))
                ->where('enabled', '1')
                ->where('business_unit_id', $unit->id)
                ->groupby('id', 'firstname', 'lastname')
                ->orderby('firstname', 'asc')
                ->get();

            $data['resources'][] = [
                'units' => $unit->description,
                'details' => $resource
            ];
        }
        $resouceId = ($request->has('resource')) ? $request->resource : NULL;

        $data['filterdate'] = [
            'from' => date('Y-m-d'),
            'to' => date('Y-m-d')
        ];

        $filter = [
            'from' => date('Y-m-d'),
            'to' => date('Y-m-d'),
            'resource' => $resouceId,
            'entity' => '',
            'brand' => '-1'
        ];

        if ($request->has('fromDate')) {
            $filter['from'] = $request->fromDate;
            $filter['to'] = $request->toDate;
        }

        if (!is_array($request->resource)) {
            $str = json_encode($request->resource);
        } else {
            $str = $request->resource;
        }

        DB::enableQueryLog();
        $pmQuery = DB::table('t_activities')
            ->select('activity', 'date_log', 't_activities.id', 'hours', 'project_id', 'project_ref', 't_activities.billable',
                DB::raw('concat(m_resources.firstname, " ", m_resources.lastname) as resource'),
                DB::raw('concat(pm.firstname, " ", pm.lastname) as ticket_manager'),
                DB::raw('(t_projects.description) as project'),
                DB::raw('(m_entities.description) as entity'),
                DB::raw('(m_brands.description) as brand'),
                DB::raw('(m_statuses.description) as status'),
                DB::raw('m_ticket_types.name AS ticket_type'),
                DB::raw('m_types.description AS task_type'))
            ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
            ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
            ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
            ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
            ->leftjoin('m_resources as pm', 'pm.id', '=', 't_projects.project_manager_id')
            ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
            ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
            ->leftJoin('m_types', 'm_types.id', '=', 't_activities.type_id')
            ->whereBetween('date_log', [$filter['from'], $filter['to']])
            ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');

        if (is_array($str)) {
            $pmQuery->whereIn('t_projects.project_manager_id', $str);
        }
        if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
            $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
        }

        if ($request->submit == "download") {
            $activityReport = $pmQuery;
            $this->makeCSV_projActLog($activityReport->distinct()->get());
        }
        $data['activities'] = $pmQuery->distinct()->paginate(20);
        $data['business_units'] = $units;

        return view('report.reportbyProjectActLogFilter', $data);
        //return view('report.test', $data);
    }

    public function fetchTicketOwner(Request $request)
    {
        $resource = DB::table('m_resources')
            ->whereIn('business_unit_id', $request->selected)
            ->get();

        return $resource;
    }

    public function RptprojActLog_fetchdata(Request $request)
    {
        if ($request->ajax()) {
            // $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
            $units = DB::table('m_business_units')
                ->select('id', 'description')
                ->where('enabled', '1')
                ->groupby('id', 'description')
                ->orderby('description', 'asc')
                ->get();

            foreach ($units as $unitKey => $unit) {
                $resource = DB::table('m_resources')
                    ->select('id', DB::raw('CONCAT(firstname," ", lastname) as name'))
                    ->where('enabled', '1')
                    ->where('business_unit_id', $unit->id)
                    ->groupby('id', 'firstname', 'lastname')
                    ->orderby('firstname', 'asc')
                    ->get();

                $data['resources'][] = [
                    'units' => $unit->description,
                    'details' => $resource
                ];
            }
            $resouceId = ($request->has('resource')) ? $request->resource : NULL;

            $filter = [
                'from' => date('Y-m-d'),
                'to' => date('Y-m-d'),
                'resource' => $resouceId,
                'entity' => '',
                'brand' => '-1'
            ];

            if ($request->has('fromDate')) {
                $filter['from'] = $request->fromDate;
                $filter['to'] = $request->toDate;
            }

            DB::enableQueryLog();
            $pmQuery = DB::table('t_activities')
                ->select('activity', 'date_log', 't_activities.id', 'hours', 'project_id', 'project_ref', 't_activities.billable',
                    DB::raw('concat(m_resources.firstname, " ", m_resources.lastname) as resource'),
                    DB::raw('concat(pm.firstname, " ", pm.lastname) as ticket_manager'),
                    DB::raw('(t_projects.description) as project'),
                    DB::raw('(m_entities.description) as entity'),
                    DB::raw('(m_brands.description) as brand'),
                    DB::raw('(m_statuses.description) as status'),
                    DB::raw('m_ticket_types.name AS ticket_type'),
                    DB::raw('m_types.description AS task_type'))
                ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
                ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                ->leftjoin('m_resources as pm', 'pm.id', '=', 't_projects.project_manager_id')
                ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                ->leftJoin('m_types', 'm_types.id', '=', 't_activities.type_id')
                ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC');

            if (trim($request->resource) != '') {
                $projmngr = explode(',', $request->resource);
                $pmQuery->whereIn('t_projects.project_manager_id', $projmngr);
            }
            // if(trim($request->brand) != '')
            // {
            // 	$brands = Brand::where('enabled', 1)->where('entity_id', $request->entity)->orderBy('description')->get();
            // 	$pmQuery = $pmQuery->where('t_activities.brand_id', $request->brand);
            // }

            if (trim($request->fromDate) != '' && trim($request->toDate) != '') {
                $pmQuery->whereBetween('t_activities.date_log', [$request->fromDate, $request->toDate]);
            }

            if ($request->submit == "download") {
                $activityReport = $pmQuery;
                $this->makeCSV_projActLog($activityReport->distinct()->get());
            }
            $data['activities'] = $pmQuery->distinct()->paginate(20);

            return view('report.reportbyProjectActLog', $data);
        }
    }

    private function makeCSV_projActLog($activities)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Ticket_Activity_Log.csv"');

        $fp = fopen('php://output', 'wb');

        $header = ["Ticket Ref", "Date Log", "Ticket Name", "Ticket Type", "Status", "Ticket Manager", "Resource", "Task Type", "Entity", "Brand", "Activity", "Hours"];
        fputcsv($fp, $header);

        foreach ($activities as $activity) {

            $ticketType = ($activity->ticket_type == NULL) ? "" : $activity->ticket_type;
            $val = [
                $activity->project_ref,
                $activity->date_log,
                $activity->project,
                $activity->ticketType,
                $activity->status,
                $activity->ticket_manager,
                $activity->resource,
                $activity->task_type,
                $activity->entity,
                $activity->brand,
                $activity->activity,
                $activity->hours
            ];
            fputcsv($fp, $val);
        }
        fclose($fp);
        exit();
    }

    public function getResource()
    {
        if (isset($_POST["selected"])) {
            $id = join("','", $_POST["selected"]);
            $query = "
		SELECT * FROM second_level_category 
		WHERE first_level_category_id IN ('" . $id . "')
		";
            $statement = $connect->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll();
            $output = '';
            foreach ($result as $row) {
                $output .= '<option value="' . $row["second_level_category_id"] . '">' . $row["second_level_category_name"] . '</option>';
            }
            echo $output;
        }
    }

    // public function reportbyPMTicket_koolreport()
    // {
    //     $report = new report;
    //     $report->run();
    //     return view("report.reportbyPMTicket",["report"=>$report]);
    // }

    // public function reportbyBusinessUnit_koolreport()
    // {
    //     $report = new report;
    //     $report->run();
    //     return view("report.reportbyPMTicket",["report"=>$report]);
    // }

}
