<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Resource;
use App\Entity;
use App\Brand;
use App\Project;
use App\Activity;
use App\Status;
use App\ProjectReference; 
use App\BusinessUnit; 

use Auth;
use DB;
use Exception;
use DateTime;

class RecentProjectsController extends Controller {

	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$statuses = Status::where('enabled', 1)->orderBy('description')->get();

		$defaultStatus = 1;

		$date_range = date('Y-m-d', strtotime("-2 week")) ."|". date("Y-m-d");

		$filter = ['status' => $defaultStatus,
					'my_recent_projects' => Auth::user()->resource_id,
					'date_range' => $date_range
				];

		if ($request->has('status')) {
			$filter['status'] = $request->status;
			$defaultStatus = $request->status;
		}

		$projects = Project::filterProject($filter);

		$request->session()->put('link_from', '/my_projects');

		return view('project.recentprojects')
			->with('projects', $projects)
			->with('statuses', $statuses)
			->with('defaultStatus', $defaultStatus);
	}


}
