<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Status;
use Auth;

class StatusController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$statuses = Status::orderBy('description')->get();

		$options = ['No', 'Yes'];

		return view('status.list', compact('options', 'statuses'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return view('status.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$request->replace(array('description' => trim($request->description)));
		
		$this->validate($request, [
			'description' => 'required|unique:m_statuses'
	    ]);

		$status = new Status;
		$status->description = $request->description;
		$status->enabled = 1;
		$status->save();

		return redirect('/status');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$status = Status::find($id);
		return view('status.edit')->with('status', $status);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
	    $this->validate($request, [
	        'description' => 'required'
	    ]);

	    $status = Status::findOrFail($id);
	    $status->description = $request->description;
	    $status->enabled = $request->has('enabled');
	    $status->save();

	    return redirect('/status');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Status::destroy($id);
		return redirect('/status')->with('success', 'Status successfully deleted!');
	}


	public function filter(Request $request)
	{
		
		$filter = array();

		$options = ['No', 'Yes'];

		if($request->has('enabled')) {
			$filter['enabled'] = $request->enabled;
		}

		$statuses = Status::filter($filter);

		return view('status.list', compact('statuses', 'options', 'filter'));
	}

}
