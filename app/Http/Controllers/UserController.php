<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User;
use App\Entity;
use App\Resource;
use Auth;
use DB;

class UserController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getRegister()
	{
		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
		return view('auth.register')
				->with('resources', $resources);
	}

	public function postRegister(Request $request)
	{
		if(Auth::user()->is_admin) {

			$request->merge(array('username' => trim($request->username)));

			$rules = ['username' => 'required|unique:m_access', 'resource_id' => 'required'];
			$this->validate($request, $rules);

			$user = new User;
			$user->username = $request->username;
			$user->password = bcrypt('password');
		    $user->resource_id = $request->resource_id;
			$user->is_admin = $request->usertype;
			$user->added_by = Auth::user()->username;
			$user->save();

			$companies = Entity::all();
			$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();

			return view('auth.register')
					->with('resources', $resources)
					->with('successmsg', 'New user successfully registered');
		} else {
			return redirect()->back()->withErrors('You don\'t have the permission to perform this task')->withInput();
		}
	}

	public function getList()
	{
		if(Auth::user()->is_admin) {
			
			$userType = ['Regular', 'Admin'];
			$options = ['Access Removed', 'Active'];

			$users = User::filter();

			return view('user.list', compact('users', 'userType', 'options'));
		} else {
			return redirect()->back()->withErrors('You don\'t have the permission to perform this task');
		}
	}

	public function getSetActive($action, $id)
	{
		if(Auth::user()->is_admin) {
			$user = User::find($id);
			$user->enabled = $action;
			$user->save();
			return redirect('/user/list');
		} else {
			return redirect()->back()->withErrors('You don\'t have the permission to perform this task');
		}
	}

	public function getChangePassword()
	{
		return view('changepassword');
	}

	public function getEdit($id)
	{
		if(Auth::user()->is_admin) {
			//$user = User::where('id', $id)->get()->first();
			$user = User::select('username', 'm_access.id', 'firstname', 'lastname', 'middlename', 
							'm_access.created_at', 'added_by', 'is_admin', 'm_access.enabled')
						->join('m_resources', 'm_resources.id', '=', 'm_access.resource_id')
						->where('m_access.id', $id)
						->orderBy('username')
						->get()
						->first();

			$types = ['0'=>'Ordinary User', '1'=>'Admin User'];
			return view('user.edit')
				->with('user',$user)
				->with('types',$types);
		} else {
			return redirect('/home')->withErrors('You don\'t have the permission to perform this task');
		}
	}

	public function postUpdate(Request $request)
	{
		$this->validate($request, [
			'id' => 'required',
			'username' => 'required'
	    ]);
	    
	    if(Auth::user()->is_admin) {
	    	$user = User::find($request->id);
		    $user->is_admin = $request->usertype;
		    $user->save();
		    return redirect('/user/list');
	    } else {
	    	return redirect('/user/list')->withErrors('You don\'t have the permission to perform this task');
	    }
	}

	public function postChangePassword(Request $request)
	{
		$rules = ['password' => 'required|min:6|confirmed',
        		'password_confirmation' => 'required|min:6'];
		$this->validate($request, $rules);

		if (Auth::attempt(['username' => Auth::user()->username, 'password' => $request->oldpassword])){
			$user = User::where('username', Auth::user()->username)->first();
			$user->password = bcrypt($request->password);
			$user->has_loggedin = 1;
			$user->save();
			return view('changepassword')->with('successmsg', 'Password successfully changed');
		} else {
			return view('changepassword')->withErrors('Incorrect password');
		}
	}

	public function getResetPassword($id)
	{
		if(Auth::user()->is_admin) {
			try {
				$user = User::findOrFail($id);
			    $user->password = bcrypt('password');
			    $user->has_loggedin = null;
			    $user->save();
			    return redirect('/user/list')->with('successmsg', 'Password successfully set to default!');
			} catch(\Exception $e) {
				return back()->withErrors('Invalid user');
			}
	    	
	    } else {
	    	return redirect('/user/list')->withErrors('You don\'t have the permission to perform this task');
	    }
	}

	public function filter(Request $request)
	{
		
		$filter = array();

		$userType = ['Regular', 'Admin'];
		$options = ['Access Removed', 'Active'];

		if ($request->has('user_type')) {
			$filter['user_type'] = $request->user_type;
		}

		if ($request->has('enabled')) {
			$filter['enabled'] = $request->enabled; 
		}

		//print_r($filter);

		$users = User::filter($filter);

		return view('user.list', compact('users', 'options', 'filter', 'userType'));
	}

}