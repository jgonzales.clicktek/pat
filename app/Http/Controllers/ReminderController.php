<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use PDF;
use Mail;
use stdClass;

use App\Resource;

class ReminderController extends Controller {

	public function reminder()
	{
		$date = date('Y-m-d'); 

		$resources = Resource::getResourceWithBelowMinLogs($date);

		foreach($resources as $resource) {

			if (!empty($resource->email)) {
				$mail = ['address' => $resource->email ];
			    $info = new stdClass();
			    $info->employee = $resource->firstname;
			    $info->total_hrs = $resource->total_hrs;

				Mail::send('email.reminder',['info' => $info], function($email) use ($mail){
		            $email->from('exad@rgoc.com.ph', 'PAT');
		            $email->to( $mail['address'] )->subject('PAT Reminder');
		            //$email->attach( $mail['attachment'] );
		        });
			}
	    }

	}


}
