<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Resource;
use App\Entity;
use App\Brand;
use App\Project;
use App\Activity;
use App\Status;
use Auth;
use DB;
use Exception;

class ReportController extends Controller {

	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
	 * Show the entity sharing report for a particular resource.
	 *
	 * @param  Request $request
	 * @return Response
	 */
	public function showSharing(Request $request)
	{
		$lastId = '';
		$name = '';
		$defaultResource = '';
		$summary = [];

		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
		$entities = Entity::where('enabled', 1)->orderBy('description')->get();

		$fromDate = $toDate = date('Y-m-d');

		if($request->has('fromDate')) {
			$fromDate = $request->fromDate;
			$toDate = $request->toDate;
		}

		DB::enableQueryLog();

		$activities = Activity::join('t_projects', 't_projects.id', '=', 't_activities.project_id')
						->join('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
						->join('m_entities', 'm_entities.id', '=', 't_projects.entity_id')
						->orderByRaw(DB::raw('firstname,lastname'))
						->select('resource_id', 'lastname', 'firstname',
							DB::raw('(m_entities.description) as entity'),
							DB::raw('SUM(hours) as total_hours'))
						->whereBetween('date_log', [$fromDate, $toDate])
						->where('t_activities.billable', 1)
						->where('m_entities.billable', 1)
						->groupBy(DB::raw('resource_id,lastname,firstname,entity'));


		if($request->has('resource')) {
			$activities->where('t_activities.resource_id', $request->resource);
			$defaultResource = $request->resource;
		}

		$result = $activities->get();

		$summary = $this->setSummary($result);
		$this->addPercentageToSummary($summary);

		return view('report.sharing')
			->with('summary', $summary)
			->with('resources', $resources)
			->with('entities', $entities)
			->with('defaultResource', $defaultResource)
			->with('fromDate', $fromDate)
			->with('toDate', $toDate);
	}

	private function setSummary($result)
	{
		$summary = [];
		$lastId = '';
		$totalHours = 0;

		if(!$result->isEmpty()) {
			foreach($result as $rst) {
				if($rst->resource_id != $lastId) {
					if($lastId !== '') {		//meaning: another resource/not new resource
						$summary[ $lastId ]['total_hours'] = $totalHours;
					}

					$name = $rst->firstname." ".$rst->lastname;
					$totalHours = 0;
				}
				$summary[ $rst->resource_id ]['name'] = $name;
				$summary[ $rst->resource_id ]['company'][$rst->entity]['hours'] = $rst->total_hours;

				$totalHours += $rst->total_hours;
				$lastId = $rst->resource_id;
			}
			$summary[ $lastId ]['total_hours'] = $totalHours;
		}

		return $summary;
	}

	private function addPercentageToSummary(&$summary)
	{
		foreach($summary as $resourceId => $val) {
			$companyPercentage = [];
			$totalPercentage = 0;

			$totalHours = $summary[$resourceId]['total_hours'];

			foreach($val['company'] as $name => $value) {
				$hours = $summary[$resourceId]['company'][ $name ]['hours'];
				$percentage = round($hours / $totalHours * 100, 2);
				$summary[$resourceId]['company'][ $name ]['percent'] = $percentage;
				$totalPercentage += $percentage;
				$companyPercentage[ $name ] = $percentage;
			}

			// echo "<br />Original percentage: ". $totalPercentage;
			// echo "<pre>";
			// print_r($summary[$resourceId]);

			if($totalPercentage != 100) {

				$this->distributeDiscrepancy($summary, $resourceId, $totalPercentage, $companyPercentage);
			}
			// echo "<br />AFter function distributeDiscrepancy: ";
			// echo "<pre>";
			// print_r($summary[$resourceId]);
			// die();

			// if($totalPercentage != 100) {
			// 	$diff = (100 - $totalPercentage);
			// 	$additionalPercentage = round($diff / count($companyPercentage), 2);

			// 	echo "<br />".$summary[$resourceId]['name']." - ".($diff);

			// 	$totalPercentage = 0;
			// 	$i = count($companyPercentage);
			// 	asort($companyPercentage);
			// 	foreach($companyPercentage as $name => $value) {
			// 		--$i;
			// 		$summary[$resourceId]['company'][ $name ]['percent'] += $additionalPercentage;
			// 		$totalPercentage += $summary[$resourceId]['company'][ $name ]['percent'];
			// 		if($i == 0 && $totalPercentage !== 100) { //last company

			// 			$summary[$resourceId]['company'][ $name ]['percent'] += (100 - $totalPercentage);
			// 		}
			// 	}
			// }
		}
	}

	private function distributeDiscrepancy(&$summary, $resourceId, $totalPercentage, $companies)
	{
		$newCompaniesArray = [];

		$diff = (100 - $totalPercentage);
		// echo "<br />Diff: ".$diff;

		asort($companies);
		$counter = count($companies);

		if($diff < 0) {					// in case of negative (over 100%)
			$key = key($companies);
			$newCompaniesArray[$key] = $diff;
		} else {
			while($counter >= 1 && $diff > 0) {
				if($counter == 1) {				//last company
					$newCompaniesArray[$key] = (100 - $totalPercentage);
				} else {
					$key = key($companies);
					$newCompaniesArray[$key] = ($diff < 0.01) ? $diff : 0.01;
					// echo "<br />key=".$key;
					$subtrahend = ($diff < 0.01) ? $diff : 0.01;
					$totalPercentage += $subtrahend;
				}

				$diff = $diff - $subtrahend;
				$counter--;
				array_shift($companies);
			}
		}

		// echo "<pre>";
		// print_r($newCompaniesArray);

		foreach($newCompaniesArray as $name => $addtlPercentage) {
			$summary[$resourceId]['company'][ $name ]['percent'] += $addtlPercentage;
		}

		// return $newCompaniesArray;

		// echo "<br />".$summary[$resourceId]['name']." - ".($diff);

		// $totalPercentage = 0;
		// $i = count($companies);
		// asort($companies);
		// foreach($companyPercentage as $name => $value) {
		// 	--$i;
		// 	$summary[$resourceId]['company'][ $name ]['percent'] += $additionalPercentage;
		// 	$totalPercentage += $summary[$resourceId]['company'][ $name ]['percent'];
		// 	if($i == 0 && $totalPercentage !== 100) { //last company

		// 		$summary[$resourceId]['company'][ $name ]['percent'] += (100 - $totalPercentage);
		// 	}
		// }

	}



}
