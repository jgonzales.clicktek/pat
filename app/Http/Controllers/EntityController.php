<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Entity;
use Auth;

class EntityController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{

		$entities = Entity::filter();
		$options = ['No', 'Yes'];

		return view('entity.list', compact('entities', 'options'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return view('entity.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$request->replace(array('description' => trim($request->description)));
		
		$this->validate($request, [
			'description' => 'required|unique:m_entities'
	    ]);

		$entity = new Entity;
		$entity->description = $request->description;
		$entity->billable = $request->billable;
		$entity->enabled = 1;
		$entity->save();

		return redirect('/entity');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$entity = Entity::find($id);
		return view('entity.edit')->with('entity', $entity);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//
		$entity = Entity::findOrFail($id);

	    $this->validate($request, [
	        'description' => 'required'
	    ]);

	    $entity->description = $request->description;
	    $entity->billable = $request->has('billable');
	    $entity->enabled = $request->has('enabled');
	    $entity->save();

	    return redirect('/entity');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Entity::destroy($id);
		return redirect('/entity')->with('success', 'Entity successfully deleted!');
	}

	public function filter(Request $request)
	{
		
		$filter = array();

		$options = ['No', 'Yes'];

		if($request->has('enabled')) {
			$filter['enabled'] = $request->enabled;
		}

		if($request->has('billable')) {
			$filter['billable'] = $request->billable;
		}

		$entities = Entity::filter($filter);

		return view('entity.list', compact('entities', 'options', 'filter'));
	}

}
