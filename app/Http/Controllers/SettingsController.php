<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Setting;
use Auth;

class SettingsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$settings = Setting::first();

		$options = ["New Scheme", "Old Scheme"]; 

		return view('settings.list', compact('options', 'settings'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$settings = Setting::find($id);
		$options = ["New Formula", "Old Formula"]; 

		return view('settings.edit', compact('options', 'settings'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{

	    $settings = Setting::findOrFail($id);
	    $settings->entity_sharing_old = $request->entity_sharing;
	    $settings->save();

	    return redirect('settings');
	}

}
