<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Resource;
use App\BusinessUnit;

use Auth;
use Exception;
use DB;

class ResourceController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$options = ['No', 'Yes'];

		$resources = Resource::orderByRaw('lastname,firstname')->get();
		$units = BusinessUnit::where('enabled', 1)->orderBy('description')->get();

		$selectedUnit = '';

		return view('resource.list', compact('resources', 'units', 'selectedUnit', 'options'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$units = BusinessUnit::where('enabled', 1)->get();
		$managers = Resource::orderByRaw('firstname')->where('enabled', 1)->get();

		return view('resource.add', compact('units','managers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// $request->replace(array('firstname' => trim($request->firstname), 
		// 						'lastname' => trim($request->lastname)));

		$this->validate($request, [
			'firstname' => 'required',
			'lastname' => 'required'
	    ]);

		try {
			if(Resource::checkUniqueName($request->firstname, $request->lastname)) {
				$name = $request->firstname." ".$request->lastname;
		    	throw new Exception($name." has already been taken", 1);
		    }

			$resource = new Resource;
			$resource->firstname = $request->firstname;
			$resource->lastname = $request->lastname;
			$resource->middlename = $request->middlename;
			$resource->email = $request->email;
			$resource->start_date = $request->has('start_date') ? $request->start_date : null;
			$resource->end_date = $request->has('end_date') ? $request->end_date : null;
			$resource->business_unit_id = $request->business_unit;
			$resource->enabled = 1;
			$resource->is_internal = $request->has('internal');
			$resource->manager_id = $request->manager;
			$resource->save();

			return redirect('/resource');

		} catch(Exception $e) {
			return redirect()->back()->withErrors($e->getMessage())->withInput();

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$resource = Resource::find($id);
		$units = BusinessUnit::where('enabled', 1)->orderBy('description')->get();
		$managers = Resource::orderByRaw('firstname')->where('id', '!=' ,$id)->where('enabled', 1)->get();

		return view('resource.edit', compact('resource', 'units', 'managers'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//
		$resource = Resource::findOrFail($id);

	    $this->validate($request, [
	        'firstname' => 'required',
	        'lastname' => 'required'
	    ]);

	    $resource->firstname = $request->firstname;
	    $resource->lastname = $request->lastname;
	    $resource->middlename = $request->middlename;
	    $resource->email = $request->email;
	    $resource->business_unit_id = $request->business_unit;
	    $resource->enabled = $request->has('enabled');
	    $resource->start_date = $request->has('start_date') ? $request->start_date : null;
		$resource->end_date = $request->has('end_date') ? $request->end_date : null;
		$resource->is_internal = $request->has('internal');
		$resource->manager_id = $request->manager;
	    $resource->save();

	    return redirect('/resource');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Resource::destroy($id);
		return redirect('/resource')->with('success', 'Resource successfully deleted!');
	}

	public function filter(Request $request)
	{
		$selectedUnit = '';
		$filter = [];

		$options = ['No', 'Yes'];

		if ($request->has('business_unit')) {
			 $selectedUnit = $request->business_unit;
			 // $filter = 
			$filter['business_unit'] = $request->business_unit;
		}

		if ($request->has('internal')) {
			$filter['internal'] = $request->internal;
		}

		if ($request->has('enabled')) {
			$filter['enabled'] = $request->enabled;
		}

		// print_r($filter);
		// DB::enableQueryLog();

		$resources = Resource::filter($filter);
		// dd(DB::getQueryLog());
		$units = BusinessUnit::where('enabled', 1)->orderBy('description')->get();

		return view('resource.list', compact('resources', 'units', 'selectedUnit', 'options', 'filter'));
	}

	public function getResources_by_businessunit(Request $request)
	{
		//$resources = Resource::where('enabled', 1)->where('business_unit_id', $request->businessunit)->orderBy('firstname')->orderBy('lastname')->get();
		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();

		return $resources->toJson(JSON_PRETTY_PRINT);
	}

	public function ViewResbyMngrs()
	{
		$units = DB::table('m_business_units')
                -> select('id','description')
                -> where('enabled', '1')
                -> groupby('id','description')
                -> orderby('description', 'asc')
                -> get();

		foreach ($units as $unitKey => $unit) 
		{
			$groupCodes = DB::table('m_resources')
					-> select('id', DB::raw('CONCAT(firstname," ", lastname) as name'),'business_unit_id')
					-> whereIn('id',['13','19','59','66','58','20'])
					-> where('enabled', '1')
					-> where('business_unit_id', $unit->id)
					-> groupby('manager_id', 'id', 'firstname', 'lastname', 'business_unit_id')
					-> orderby('firstname', 'asc')
					-> get();

			$data['resource'][$unitKey]['unit'] = $unit->description;

			foreach ($groupCodes as $key => $groupCode) 
			{
				$GroupCondition = 
				[
					'business_unit_id' => $groupCode->business_unit_id,
					'manager_id' => $groupCode->id
				];	

				$subordinates = DB::table('m_resources')
						-> select('id', DB::raw('CONCAT(firstname," ", lastname) as name'))
						-> where('enabled', '1')
						-> where($GroupCondition)
						-> groupby('manager_id', 'id', 'firstname', 'lastname')
						-> orderby('firstname', 'asc')
						-> get();
						//-> toArray();		

				$data['resource'][$unitKey]['manager'] = $groupCode->name;
				
				foreach ($subordinates as $key => $subordinate) 
				{ 
					$GroupCon = 
					[
						'business_unit_id' => $groupCode->business_unit_id,
						'manager_id' => $subordinate->id
					];	

					$TLsubordinate = DB::table('m_resources')
						-> select('id', DB::raw('CONCAT(firstname," ", lastname) as name'))
						-> where('enabled', '1')
						-> where($GroupCon)
						-> groupby('manager_id', 'id', 'firstname', 'lastname')
						-> orderby('firstname', 'asc')
						-> get();
						//-> toArray();		

					$data['resource'][$unitKey]['TLsubordinates'][] = [
						'subordinates' => $subordinate->name,
						'TLsub' => $TLsubordinate
					];
				}
			}
		}
	//dd($data);
		return view('resource.resbymngrs', $data);
	}

}
