<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use PDF;
use Mail;
use stdClass;
use App\Library\Jdax;
use App\Library\JdaxMySQL;
use App\Library\JdaxDB2;

class ScraperController extends Controller {

	public function getTest()
	{
		$mysqlConnStr = ['hostname' => '172.16.1.83', 
						'username'=>'ccri_user',
						'password' => 'asrccritheman',
						'database' => 'asr_ccri' ];
		$db2ConnStr = ['hostname'=>'jdaprod1.rgoc.com.ph', 
						'username'=>'itd01',
						'password' => 'itd01',
						'database' => 'MMGAPLIB' ];
		
		$mysqlDB = new JdaxMySQL($mysqlConnStr);
		$db2DB = new JdaxDB2($db2ConnStr);

		$merchandising = $mysqlDB->getMerchandising();
		if(!empty($merchandising)) {
			foreach($merchandising as $merch) {
				echo $merch->brand."<br />";
			}
		}

		$details = $db2DB->getOutOfStock();
		if(!empty($details)) {
			$SKU_prev = '';
			$stores = [];
			$data = [];
			$i = -1;
			foreach($details as $detail) {
				$stores[$detail->ISTORE] = $detail->DPTNAM;
				$brand = trim($detail->DPTNAM);
				if($SKU_prev != $detail->INUMBR) {
					$data[$brand][++$i]['style'] = $detail->ZSTYDS;
					$data[$brand][$i]['upc'] = $detail->IUPC;
					$data[$brand][$i]['size'] = $detail->SIZDSC;
					$data[$brand][$i]['description'] = $detail->IDESCR;
					$data[$brand][$i]['warehouse'] = $detail->ONHANDWH;
				}
				$data[$brand][$i]['store'][] = ['storeno'=>$detail->ISTORE, 'onhand'=>$detail->STOREONHAND];

				$SKU_prev = $detail->INUMBR;
			}
		}
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}

	public function getStockpdf($brand)
	{
		$brand = strtoupper($brand);

		$mysqlConnStr = ['hostname' => '172.16.1.83', 
						'username'=>'ccri_user',
						'password' => 'asrccritheman',
						'database' => 'asr_ccri' ];
		$db2ConnStr = ['hostname'=>'jdaprod1.rgoc.com.ph', 
						'username'=>'itd01',
						'password' => 'itd01',
						'database' => 'MMGAPLIB' ];
		
		$mysqlDB = new JdaxMySQL($mysqlConnStr);
		$db2DB = new JdaxDB2($db2ConnStr);
		
		$merchandising = $mysqlDB->getEmails();
		
		if(!empty($merchandising)) {
			$merchInfo = [];
			$i = 0;
			$prev_brand = '';
			foreach($merchandising as $merch) {
				
				($prev_brand == $merch->brand) ? $i++ :	$i = 0;

				$merchInfo[$merch->brand][$i]['email'] = $merch->email;
				$merchInfo[$merch->brand][$i]['name'] = $merch->msdname;
				
				$prev_brand = $merch->brand;
			}
			// echo "<pre>";
			// print_r($merchInfo);
		} 

		if (!empty($merchInfo)) {
		  foreach($merchInfo[$brand] as $merch_info) {
		    	//$mail = ['address' => $email_address, 'attachment' => $pathtofile];
		    	$mail = ['address' => $merch_info['email'] ];
			    $info = new stdClass();
			    $info->employee = $merch_info['name'];
			    $info->brand = $brand;

				Mail::send('emails.report',['info' => $info], function($email) use ($mail){
		            $email->from('exad@rgoc.com.ph', 'ASR');
		            $email->to( $mail['address'] )->subject('Out of Stock Report');
		            //$email->attach( $mail['attachment'] );
		        });
		    }
		}

		// $details = $db2DB->getOutOfStock($brand);

		// if(!empty($details)) {
		// 	$data = [];
		// 	$i = -1;
		// 	$totalInventory = 0;
		// 	$totalOnhand = 0;

		// 	foreach($details as $detail) {
		// 		$brand = trim($detail->DEPARTMENT);
		// 		$data[$brand][++$i]['style'] = $detail->ZSTYDS;
		// 		$data[$brand][$i]['division'] = $detail->DIVISION;
		// 		$data[$brand][$i]['upc'] = $detail->IUPC;
		// 		$data[$brand][$i]['size'] = $detail->SIZDSC;
		// 		$data[$brand][$i]['description'] = $detail->IDESCR;
		// 		$data[$brand][$i]['inventory'] = $detail->INVENTORY;
		// 		$data[$brand][$i]['onhand'] = $detail->ONHAND;
		// 		$totalInventory += (int)$detail->INVENTORY;
		// 		$totalOnhand += (int)$detail->ONHAND;
		// 	}
		// }
		// print_r($data);

		if(isset($data)) {
			foreach ($data as $brand => $items) {
/*
				$html = '<table border="1" cellpadding="4" cellspacing="1" width="100%">';
				$html .= '<tr>';
				$html .= '<th>CATEGORY/BRAND</th>';
				$html .= '<th>DIVISION</th>';
				$html .= '<th>STYLE</th>';
				$html .= '<th>UPC</th>';
				$html .= '<th>SIZE</th>';
				$html .= '<th>DESCRIPTION</th>';
				$html .= '<th>MINIMUM INVENTORY</th>';
				$html .= '<th>ON HAND</th>';
				$html .= '</tr>';
				foreach($items as $key => $item) {
					$html .= '<tr>';
					$html .= "<td>{$brand}</td>";
					$html .= "<td>{$item['division']}</td>";
					$html .= "<td>{$item['style']}</td>";
					$html .= "<td>{$item['upc']}</td>";
					$html .= "<td>{$item['size']}</td>";
					$html .= "<td>{$item['description']}</td>";
					$html .= "<td>".(int)$item['inventory']."</td>";
					$html .= "<td>".(int)$item['onhand']."</td>";
					
					$locOnHand = '';
					
					$html .= '</tr>';
				}
				$html .= '<tr>';
				$html .= '<th colspan="6">Total</th>';
				$html .= "<td>$totalInventory</td>";
				$html .= "<td>$totalOnhand</td>";
				$html .= '</tr>';

				$html .= '</table>';
				
				$frame = '<table width="100%">';
				$frame .= '<tr><td width="100%" align="center" colspan="2">CASUAL CLOTHING RETAILERS, INC.</td></tr>';
				$frame .= '<tr><td width="100%" align="center" colspan="2">AUTOMATED STOCK REPLENISHMENT</td></tr>';
				$frame .= '<tr><td width="100%" align="center" colspan="2"> </td></tr>';
				$frame .= '<tr><td width="100%" align="center" colspan="2">OUT OF STOCK REPORT</td></tr>';
				$frame .= '<tr>';
				$frame .= '<td width="80%">Print Date & Time: '.date('m/d/Y h:i:s A').'</td>';
				$frame .= '<td>***SYSTEM GENERATED***</td>';
				$frame .= '</tr>';
				$frame .= '<tr><td width="100%" align="center" colspan="2">'.$html.'</td></tr>';
				$frame .= '</table>';

				//echo $frame;

				$password = ''; 
				PDF::SetProtection(array('copy'), $password, null, 0, null);
				PDF::SetTitle('Out of Stock Report');
				PDF::setPageOrientation('L');
				PDF::AddPage();
			    PDF::writeHTML($frame, true, false, true, false, '');
			    $pathtofile = storage_path() . '/out_of_stock_'.$brand.'.pdf';
			    PDF::Output($pathtofile,'F');
*/
				//=============================== EMAIL ==================================
			    //$mail = ['address' => $payroll->external_email, 'attachment' => $pathtofile];
			    
			    //$email_addresses = explode(",", $merchInfo[$brand]['email']);
/*
			    foreach($merchandising as $merch) {
					//echo $merch->email."<br />";
					$merchInfo[$merch->brand]['email'] = $merch->email;
					$merchInfo[$merch->brand]['name'] = $merch->msdname;
				}
*/
				//==========================================
			  //   foreach($merchInfo[$brand] as $merch_info) {
			  //   	//$mail = ['address' => $email_address, 'attachment' => $pathtofile];
			  //   	$mail = ['address' => $merch_info['email'] ];
				 //    $info = new stdClass();
				 //    $info->employee = $merch_info['name'];
				 //    $info->brand = $brand;

					// Mail::send('emails.report',['info' => $info], function($email) use ($mail){
			  //           $email->from('exad@rgoc.com.ph', 'ASR');
			  //           $email->to( $mail['address'] )->subject('Out of Stock Report');
			  //           //$email->attach( $mail['attachment'] );
			  //       });
			  //   }
			  //   =========================================
		        //sleep(5);
				//=============================== EMAIL ==================================
			}
		}



	}
}
