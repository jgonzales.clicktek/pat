<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Entity;
use App\BusinessUnit;
use App\Resource;
use Auth;

class BusinessUnitController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$units = BusinessUnit::filter();
		$options = ['No', 'Yes'];

		return view('unit.list', compact('units', 'options'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$managers = Resource::orderByRaw('firstname')->where('enabled', 1)->get();

		return view('unit.add', compact('managers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$request->replace(array('description' => trim($request->description)));
		
		$this->validate($request, [
			'description' => 'required|unique:m_business_units'
	    ]);

		$unit = new BusinessUnit;
		$unit->description = $request->description;
		$unit->manager_id = $request->manager;
		$unit->enabled = 1;
		$unit->save();

		return redirect('unit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$managers = Resource::orderByRaw('firstname')->where('enabled', 1)->get();

		//
		$unit = BusinessUnit::find($id);
		return view('unit.edit', compact('unit','managers'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//
		$unit = BusinessUnit::findOrFail($id);

	    $this->validate($request, [
	        'description' => 'required'
	    ]);

	    $unit->description = $request->description;
		$unit->manager_id = $request->manager;
	    $unit->enabled = $request->has('enabled');
	    $unit->save();

	    return redirect('/unit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		BusinessUnit::destroy($id);
		return redirect('/unit')->with('success', 'Business unit successfully deleted!');
	}

	public function filter(Request $request)
	{
		
		$filter = array();

		$options = ['No', 'Yes'];

		if($request->has('enabled')) {
			$filter = ['enabled' => $request->enabled];
		}

		if($request->has('billable')) {
			$filter = ['billable' => $request->billable];
		}

		$units = Entity::filter($filter);

		return view('unit.list', compact('units', 'options', 'filter'));
	}

}
