<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Resource;
use App\Entity;
use App\Brand;
use App\Project;
use App\Activity;
use App\Status;
use App\ProjectReference; 
use App\BusinessUnit; 
use App\TicketType; 

use Auth;
use DB;
use Exception;
use DateTime;

class ProjectController extends Controller {

	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
		$statuses = Status::where('enabled', 1)->orderBy('description')->get();

		$defaultStatus = 1;
		$defaultProjmngr = -1; // Auth::user()->resource_id;
		$defaultResource = -1; // Auth::user()->resource_id;

		$filter = ['status' => $defaultStatus];

		$projects = Project::filterProject($filter);

		$projects = collect($projects)->sortByDesc('project_ref');

		$request->session()->put('link_from', '/project');

		return view('project.list')
			->with('resources', $resources)
			->with('projects', $projects)
			->with('statuses', $statuses)
			->with('defaultProjmngr', $defaultProjmngr)
			->with('defaultStatus', $defaultStatus)
			->with('defaultResource', $defaultResource);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		$project = Project::findOrFail($id);

		if(!Auth::user()->is_admin && (Auth::user()->username != $project->added_by)) {
			return back()->withErrors('You don\'t have the right to perform this task!');
		}

		Activity::where('project_id', $id)->delete();
		Project::destroy($id);

		$link = $request->session()->get('link_from');

		return redirect($link)->with('success', 'Project successfully deleted!');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function addProject()
	{
		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
		$entities = Entity::where('enabled', 1)->orderBy('description')->get();
		$statuses = Status::where('enabled', 1)->orderBy('description')->get();
		$ticketTypes = TicketType::where('enabled', 1)->get();
		//$statuses = ['Open', 'Closed', 'Whatever', 'Don\'t Know'];

		return view('project.add')
			->with('resources', $resources)
			->with('entities', $entities)
			->with('statuses', $statuses)
			->with('ticketTypes', $ticketTypes)
			->with('defVal', '');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request $request
	 * @return Response
	 */
	public function saveProject(Request $request)
	{
		//dd($request);
		$this->validate($request, [
			'project' => 'required',
			'start' => 'date',
			'end' => 'date',
			'closed' => 'date',
			'hours' => 'numeric',
	    ]);

		try {

			$year = '';

			if($request->has('start')) {
				$date = DateTime::createFromFormat("Y-m-d", $request->start);
				$year = $date->format("Y");
			}

			$projectRef = ProjectReference::generateProjectRef($year);

			$project = new Project;
			$project->description = $request->project;
			$project->entity_id = ($request->entity) ? $request->entity : null;
			$project->ticket_type_id = ($request->ticket_type) ? $request->ticket_type : null;
			$project->project_manager_id = ($request->projmngr) ? $request->projmngr : null;
			$project->start_date = ($request->start) ? $request->start : null;
			$project->end_date = ($request->end) ? $request->end : null;
			$project->closed_date = ($request->closed) ? $request->closed : null;
			$project->status_id = ($request->status) ? $request->status : null;
			$project->estimate_hours = ($request->estimate) ? $request->estimate : null;
			$project->repository = ($request->repository) ? $request->repository : null;
			$project->remarks = ($request->remarks) ? $request->remarks : null;
			$project->added_by = Auth::user()->username;
			$project->project_ref = $projectRef;
			$project->save();

			return redirect('/project')->with('success', 'Project successfully created!');
		} catch(\Exception $e) {
			return back()->withErrors($e->getMessage());

			// $resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
			// $entities = Entity::where('enabled', 1)->orderBy('description')->get();
			// $brands = Brand::where('enabled', 1)->orderBy('description')->get();
			// $statuses = Status::where('enabled', 1)->orderBy('description')->get();
			// $ticketTypes = TicketType::where('enabled', 1)->get();

			// return view('project.add')
			// 	->with('resources', $resources)
			// 	->with('entities', $entities)
			// 	->with('brands', $brands)
			// 	->with('statuses', $statuses)
			// 	->withErrors($e->getMessage());
	    }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showProjectDetails($id, Request $request)
	{

		$resources = Resource::where('enabled', 1)->orderByRaw('firstname, lastname')->get();
		$entities = Entity::where('enabled', 1)->orderBy('description')->get();
		$statuses = Status::where('enabled', 1)->orderBy('description')->get();
		$project = Project::findOrFail($id);
		$units = BusinessUnit::orderBy('description')->get();
		$ticketTypes = TicketType::where('id', $project->ticket_type_id)->get();

		$filter = [];
		$defaultResource = ''; //Auth::user()->resource_id;
		$selectedUnit = '';

		if($request->has('resource')) {
			$filter['resource'] = $request->resource;
			$defaultResource = $request->resource;
		}

		if($request->has('business_unit')) {
			$filter['business_unit'] = $request->business_unit;
			$selectedUnit = $request->business_unit;
		}

		$activities = Activity::getActivities($id, $filter);
		// $activities->paginate(10);
		$activities->setPath('');

		if ($request->has('resource')) {
			$activities->appends(['resource' => $request->resource]);
		}

		$request->session()->put('link_from', '/project/details/'.$id);

		$defVal = '';
		
		return view('project.details', compact('project', 'resources', 'defaultResource', 'entities', 'statuses', 'activities', 'defVal', 'units', 'selectedUnit','ticketTypes'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateProject(Request $request)
	{
		$this->validate($request, [
			'project' => 'required',
			'start' => 'date',
			'end' => 'date',
			'closed' => 'date'
	    ]);

		try {

			$project = Project::findOrFail($request->project_id);

// ======================================================================== \\
			// $year = '';
			// if($request->has('start')) {
			// 	$date = DateTime::createFromFormat("Y-m-d", $request->start);
			// 	$year = $date->format("Y");
			// }

			// $projectRef = ProjectReference::generateProjectRef($year);
// ======================================================================== \\

			if(!($project->total_hours > 0)) {
				$project->entity_id = ($request->entity) ? $request->entity : null;
			}

			$project->description = $request->project;
			$project->project_manager_id = ($request->projmngr) ? $request->projmngr : null;
			$project->start_date = ($request->start) ? $request->start : null;
			$project->end_date = ($request->end) ? $request->end : null;
			$project->closed_date = ($request->closed) ? $request->closed : null;
			$project->status_id = ($request->status) ? $request->status : null;
			$project->estimate_hours = ($request->estimate) ? $request->estimate : null;
			$project->repository = ($request->repository) ? $request->repository : null;
			$project->ticket_type_id = ($request->ticket_type) ? $request->ticket_type : null;
			$project->remarks = ($request->remarks) ? $request->remarks : null;
// ======================================================================== \\
			// $project->project_ref = $projectRef;
// ======================================================================== \\
			$project->save();

			return redirect('/project')->with('success', 'Project successfully created!');

		} catch(\Exception $e) {
			return redirect('/project')->withErrors($e->getMessage());
	    }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editProject($id, Request $request)
	{
		try {
			$project = Project::findOrFail($id);

			// $ticketTypes = TicketType::where('id', $project->ticket_type_id)->get();
			$ticketTypes = TicketType::where('enabled', 1)->get();

			// if($project->total_hours > 0) {
			// 	throw new Exception('Editing this project is not permitted!');
			// }

			$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
			$entities = Entity::where('enabled', 1)->orderBy('description')->get();

			$statuses = Status::where('enabled', 1)->orderBy('description')->get();

			$link = $request->session()->get('link_from');

			//dd($project->description);

			return view('project.edit')
				->with('project', $project)
				->with('resources', $resources)
				->with('entities', $entities)
				->with('statuses', $statuses)
				->with('cancelUrl', $link)
				->with('ticketTypes', $ticketTypes)
				->with('defVal', '');

		} catch(\Exception $e) {
			return back()->withErrors($e->getMessage());
		}
	}

	/**
	 * Show the form for logging the activity for a particular project.
	 *
	 * @param  int  $projectId
	 * @return Response
	 */
	public function filterDisplay(Request $request)
	{
		$defaultProjmngr = '';
		$defaultResource = '';
		$defaultStatus = '';
		$filter = [];

		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
		$statuses = Status::where('enabled', 1)->orderBy('description')->get();

		if($request->has('projmngr')) {
			$filter['projmngr'] =  $request->projmngr;
			$defaultProjmngr = $request->projmngr;
		}

		if($request->has('resource')) {
			$filter['resource'] =  $request->resource;
			$defaultResource = $request->resource;
		}

		if($request->has('status')) {
			$filter['status'] =  $request->status;
			$defaultStatus = $request->status;
		}

		$projects = Project::filterProject($filter);

		return view('project.list')
			->with('resources', $resources)
			->with('statuses', $statuses)
			->with('projects', $projects)
			->with('defaultProjmngr', $defaultProjmngr)
			->with('defaultStatus', $defaultStatus)
			->with('defaultResource', $defaultResource);
	}

	public function showMyProjects(Request $request)
	{
		$statuses = Status::where('enabled', 1)->orderBy('description')->get();

		$defaultStatus = 1;

		$filter = ['resource' => Auth::user()->resource_id, 
					'myprojects' => Auth::user()->resource_id,
					'status' => $defaultStatus];

		if ($request->has('status')) {
			$filter['status'] = $request->status;
			$defaultStatus = $request->status;
		}

		$projects = Project::filterProject($filter);

		$request->session()->put('link_from', '/my_projects');

		return view('project.myprojects')
			->with('projects', $projects)
			->with('statuses', $statuses)
			->with('defaultStatus', $defaultStatus);
	}

	public function filter(Request $request)
	{

		$filter = [];

		if($request->has('projmngr')) {
			$filter['projmngr'] =  $request->projmngr;
		}

		if($request->has('resource')) {
			$filter['resource'] =  $request->resource;
		}

		if($request->has('status')) {
			$filter['status'] =  $request->status;
		}

		$projects = Project::filterProject($filter);

		return \Response::json($projects);
	}

}
