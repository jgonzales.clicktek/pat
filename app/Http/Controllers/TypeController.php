<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Type;
use Auth;

class TypeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$types = Type::orderBy('description')->get();
		$options = ['No', 'Yes'];

		return view('type.list', compact('options', 'types'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return view('type.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$request->replace(array('description' => trim($request->description)));

		$this->validate($request, [
			'description' => 'required|unique:m_types'
	    ]);

		$type = new Type;
		$type->description = $request->description;
		$type->enabled = 1;
		$type->save();

		return redirect('/type');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$type = Type::find($id);
		return view('type.edit')->with('type', $type);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
	    $this->validate($request, [
	        'description' => 'required'
	    ]);

	    $type = Type::findOrFail($id);
	    $type->description = $request->description;
	    $type->enabled = $request->has('enabled');
	    $type->save();

	    return redirect('/type');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Type::destroy($id);
		return redirect('/type')->with('success', 'Type successfully deleted!');
	}


	public function filter(Request $request)
	{
		
		$filter = array();

		$options = ['No', 'Yes'];

		if($request->has('enabled')) {
			$filter['enabled'] = $request->enabled;
		}

		$types = Type::filter($filter);

		return view('type.list', compact('types', 'options', 'filter'));
	}

}
