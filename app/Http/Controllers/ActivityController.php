<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Resource;
use App\Project;
use App\Type;
use App\Activity;
use App\Brand;
use App\Entity;
use App\BusinessUnit;
use Auth;
use DB;

class ActivityController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the form for logging the activity for a particular project.
	 *
	 * @param  int  $projectId
	 * @return Response
	 */
	public function addActivity($projectId, Request $request)
	{
		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
		$project = Project::findOrFail($projectId);
		$brands = Brand::where('enabled', 1)->where('entity_id', $project->entity_id)
					->orderBy('description')->get();
		$types = Type::where('enabled', 1)->orderBy('description')->get();
		$entities = Entity::where('enabled', 1)->orderBy('description')->get();

		$link = $request->session()->get('link_from');

		return view('activity.add')
			->with('project', $project)
			->with('brands', $brands)
			->with('entities', $entities)
			->with('types', $types)
			->with('cancelUrl', $link)
			->with('resources', $resources);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Resource $resource
	 * @return Response
	 */
	public function saveActivity(Request $request)
	{
		$this->validate($request, [
			'date_log' => 'required|date',
			'type' => 'required',
			'entity' => 'required',
			'brand' => 'required',
			'activity' => 'required',
			'resource' => 'required',
			'hours' => 'required|numeric'
	    ]);

	    $activity = new Activity;
	    $activity->project_id = $request->project_id;
	    $activity->entity_id = $request->entity;
	    $activity->brand_id = $request->brand;
	    $activity->date_log = $request->date_log;
	    $activity->type_id = $request->type;
	    $activity->activity = $request->activity;
	    $activity->resource_id = $request->resource;
	    $activity->hours = $request->hours;
	    $activity->billable = ($request->billable) ? $request->billable : 0;
	    $activity->save();

	    $this->updateProjectTotalHours($request->project_id);

	    return redirect('project/details/'.$request->project_id);
	}

	private function updateProjectTotalHours($projectId)
	{
		$totalHrs = Activity::select('hours')->where('project_id', $projectId)->get()
	    				->sum('hours');

	    $project = Project::findOrFail($projectId);

	    $project->total_hours = $totalHrs;
	    $project->save();
	}

	/**
	 * Show the form for logging the activity for a particular project.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editActivity($id, Request $request)
	{
		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();
		$types = Type::where('enabled', 1)->orderBy('description')->get();
		$activity = Activity::findOrFail($id);
		$project = Project::findOrFail($activity->project_id);
		$brands = Brand::where('enabled', 1)->where('entity_id', $activity->entity_id)
						->orderBy('description')->get();
		$entities = Entity::where('enabled', 1)->orderBy('description')->get();

		$link = 'project/details/'.$id;

		if($request->session()->has('link_from')) {
			$link = $request->session()->get('link_from');
		}

		return view('activity.edit')
			->with('activity', $activity)
			->with('project', $project)
			->with('brands', $brands)
			->with('entities', $entities)
			->with('types', $types)
			->with('cancelUrl', $link)
			->with('resources', $resources);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateActivity(Request $request)
	{
		$this->validate($request, [
			'date_log' => 'required|date',
			'type' => 'required',
			'activity' => 'required',
			'resource' => 'required',
			'hours' => 'required|numeric'
	    ]);

		try {
			$activity = Activity::findOrFail($request->activity_id);
			$activity->project_id = $request->project_id;
			$activity->entity_id = $request->entity;
			$activity->brand_id = $request->brand;
		    $activity->date_log = $request->date_log;
		    $activity->type_id = $request->type;
		    $activity->activity = $request->activity;
		    $activity->resource_id = $request->resource;
		    $activity->hours = $request->hours;
		    $activity->billable = ($request->billable) ? $request->billable : 0;
		    $activity->save();

		    $this->updateProjectTotalHours($request->project_id);

		    $link = 'project/details/'.$request->project_id;

			if($request->session()->has('link_from')) {
				$link = $request->session()->get('link_from');
			}

			return redirect($link);

		} catch(\Exception $e) {
				return back()->withErrors($e->getMessage());
	    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		$activity = Activity::findOrFail($id);
		$project_id = $activity->project_id;

		if((!Auth::user()->is_admin) && (Auth::user()->resource_id != $activity->resource_id)) {
			return redirect('/project/details/'.$project_id)->withErrors('You don\'t have the right to perform this task!');
		}

		Activity::destroy($id);

		$this->updateProjectTotalHours($project_id);

		$link = '/project/details/'.$project_id;

		if($request->session()->has('link_from')) {
			$link = $request->session()->get('link_from');
		}

		return redirect($link)->with('success', 'Project successfully deleted!');
	}

	public function showSummary(Request $request)
	{
		$summary = [];
		$months = [];
		$lastId = '';
		$selectedResource = -1;
		$includeAdmin = 0;

		$filter['year'] = date('Y');
		if ($request->has('year')) {
			$filter['year'] = $request->year;
		}

		if ($request->has('business_unit')) {
			$filter['business_unit'] = $request->business_unit;
		}

		if ($request->has('resource') && $request->resource >= 0) {
			$selectedResource = $filter['resource'] = $request->resource;
		}

		if ($request->has('includeAdmin')) {
			$result = Activity::getAnnualSummary($filter);
			$includeAdmin = 1;
		} else {
			$result = Activity::getAnnualBillableSummary($filter);
		}

		foreach($result as $rst) {
			if($rst->id != $lastId) {
				$name = $rst->firstname." ".$rst->lastname;
			}

			$summary[ $name ][$rst->buwan] = $rst->total_hours;
			$summary[ $name ]['id'] = $rst->id;

			$lastId = $rst->id;
		}

		$years = Activity::select(DB::raw('DISTINCT YEAR(date_log) as year'))->orderBy('year', 'DESC')->get();

		$monthName = cal_info(0);
		
		for($i = 1; $i <= 12 ; $i++) {
			$months[$i] = $monthName['abbrevmonths'][$i];
		}

		$resources = ['Outsource', 'Internal'];
		$selectedYear = $filter['year'];
		
		$units = BusinessUnit::where('enabled', 1)->orderBy('description')->get();
		
		return view('project.summary', compact('summary', 'years', 'months', 'selectedYear', 'resources', 'selectedResource', 'includeAdmin', 'units', 'filter'));
	}

	public function viewMyActivity(Request $request)
	{
		$fromDate = $toDate = date('Y-m-d');

		$resources = Resource::where('enabled', 1)->orderBy('firstname')->orderBy('lastname')->get();

		if($request->has('fromDate')) {
			$fromDate = $request->fromDate;
			$toDate = $request->toDate;
		}

		$resouceId = ($request->has('resource')) ? $request->resource : Auth::user()->resource_id;

		DB::enableQueryLog();

		$activities = Activity::where('resource_id', $resouceId)
						->leftJoin('m_types', 'm_types.id', '=', 't_activities.type_id')
						->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
						->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
						->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
						->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
						->orderByRaw('t_activities.date_log DESC,t_activities.id DESC')
						->select('t_activities.id','hours', 'activity', 'date_log', 'project_id',
							DB::raw('(t_projects.description) as project'),
							DB::raw('(m_entities.description) as entity'),
							DB::raw('(m_brands.description) as brand'),
							DB::raw('(m_statuses.description) as status'),
							DB::raw('(m_types.description) as act_type'))
						->whereBetween('date_log', [$fromDate, $toDate])
						->get();

		// dd(DB::getQueryLog());
		// die();

		$request->session()->put('link_from', '/my_activity');

		return view('activity.myactivity')
			->with('activities', $activities)
			->with('resources', $resources)
			->with('defaultResource', $resouceId)
			->with('fromDate', $fromDate)
			->with('toDate', $toDate);
	}

	public function showMonthly($month, $year, $id)
	{
		$result = [];
		$month = sprintf("%02d", $month);
		$numberOfDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

		$resource = Resource::find($id);

		$activities = Activity::getMonthlyActivity($year, $month, $id);

		foreach($activities as $activity) {
			$daysLogs[$activity->date_log] = $activity->total_hours;
		}

		for($i = 1; $i <= $numberOfDays; $i++) {
			$date = $year.'-'.$month.'-'.sprintf("%02d", $i);
			// $result[$date] = (isset($daysLogs[$date])) ? $daysLogs[$date] : "";
			$result[$i] = (isset($daysLogs[$date])) ? $daysLogs[$date] : "";
		}
		
		// $calendar = $this->draw_calendar($month, $year, $result);

		$monthName = date("F", mktime(null, null, null, $month));
		$runningDay = date('w',mktime(0,0,0,$month,1,$year));
		// $daysInMonth = date('t',mktime(0,0,0,$month,1,$year));
		$days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

		return view('activity.monthly')
				->with('runningDay', $runningDay)
				->with('daysInMonth', $numberOfDays)
				->with('days', $days)
				->with('data', $result)
				->with('resource', $resource)
				->with('year', $year)
				->with('monthName', $monthName);

	}

	private function draw_calendar($month, $year, $data){

		/* draw table */
		$calendar = '<table cellpadding="0" cellspacing="0" class="calendar table">';

		/* table headings */
		$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
		$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

		/* days and weeks vars now ... */
		$running_day = date('w',mktime(0,0,0,$month,1,$year));
		$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		$days_in_this_week = 1;
		$day_counter = 0;

		/* row for week one */
		$calendar.= '<tr class="calendar-row">';

		/* print "blank" days until the first of the current week */
		for($x = 0; $x < $running_day; $x++):
			$calendar.= '<td class="calendar-day-np"> </td>';
			$days_in_this_week++;
		endfor;

		/* keep going with days.... */
		for($list_day = 1; $list_day <= $days_in_month; $list_day++):
			$calendar.= '<td class="calendar-day">';
				/* add in the day number */
				$calendar.= '<div class="day-number">'.$list_day.'</div>';

				/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
				$calendar.= '<p>'.$data[$list_day].' </p>';
				
			$calendar.= '</td>';
			if($running_day == 6):
				$calendar.= '</tr>';
				if(($day_counter+1) != $days_in_month):
					$calendar.= '<tr class="calendar-row">';
				endif;
				$running_day = -1;
				$days_in_this_week = 0;
			endif;
			$days_in_this_week++; $running_day++; $day_counter++;
		endfor;

		/* finish the rest of the days in the week */
		if($days_in_this_week < 8):
			for($x = 1; $x <= (8 - $days_in_this_week); $x++):
				$calendar.= '<td class="calendar-day-np"> </td>';
			endfor;
		endif;

		/* final row */
		$calendar.= '</tr>';

		/* end the table */
		$calendar.= '</table>';
		
		/* all done, return result */
		return $calendar;
	}

}
