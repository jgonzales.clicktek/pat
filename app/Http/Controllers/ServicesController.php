<?php

use App\Http\Requests;
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Auth;

class ServicesController extends Controller
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
        //
        $services = Service::filter();
		$options = ['No', 'Yes'];

		return view('services.list', compact('services', 'options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('services.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->replace(array('description' => trim($request->description)));
		
		$this->validate($request, [
			'description' => 'required|unique:m_services'
	    ]);

		$service = new Service;
		$service->description = $request->description;
		$service->billable = $request->billable;
		$service->enabled = 1;
		$service->save();

		return redirect('/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $services = Service::find($id);
		return view('services.edit')->with('services', $services);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $services = Service::findOrFail($id);

	    $this->validate($request, [
	        'description' => 'required'
	    ]);

	    $services->description = $request->description;
	    $services->billable = $request->has('billable');
	    $services->enabled = $request->has('enabled');
	    $services->save();

	    return redirect('/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Service::destroy($id);
		return redirect('/services')->with('success', 'Services successfully deleted!');
    }

    public function filter(Request $request)
	{
		
		$filter = array();

		$options = ['No', 'Yes'];

		if($request->has('enabled')) {
			$filter['enabled'] = $request->enabled;
		}

		if($request->has('billable')) {
			$filter['billable'] = $request->billable;
		}

		$services = Service::filter($filter);

		return view('services.list', compact('services', 'options', 'filter'));
	}
}
