<?php namespace App\Http\Controllers;

use Auth;

class TestController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function test()
	{
		$url="http://testapp.rgoc.com.ph/brand";

		$result = file_get_contents($url);

		echo "<pre>";
		print_r($result);

	}

}
