<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

// use Illuminate\Http\Request;

class AuthenticateAccessedModule
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = \Request::path();

        $allowed = false;

        if (null !== (session('modules'))) {
            $modules = session('modules');
            foreach ($modules as $key => $module) {
                for ($n = 0; $n < count($module); $n++) {
                    if ($route == $module[$n]['route']) {
                        $allowed = true;
                    }
                }
            }

            if (! $allowed) {
                return redirect('home')->withErrors('Unauthorized access')->send();
            }
        }

        return $next($request);
    }

}