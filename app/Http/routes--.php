<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('home', 'HomeController@index');

Route::get('test', 'TestController@test');

Route::controller('user', 'UserController');

Route::get('reminder', 'ReminderController@reminder');

Route::get('project', 'ProjectController@index');
Route::get('project/add', 'ProjectController@addProject');
Route::post('project/add', 'ProjectController@saveProject');
Route::get('project/edit/{id}', 'ProjectController@editProject');
Route::match(['get', 'post'], 'project/details/{id}', 'ProjectController@showProjectDetails');
Route::post('project/update', 'ProjectController@updateProject');
Route::get('project/del/{id}', 'ProjectController@destroy');
Route::post('project/search', 'ProjectController@filterDisplay');
Route::match(['get', 'post'], 'my_projects', 'ProjectController@showMyProjects');
Route::match(['get', 'post'], 'project/filter', 'ProjectController@filter');
Route::match(['get', 'post'], 'recent_projects', 'RecentProjectsController@index');

Route::get('activity/add/{id}', 'ActivityController@addActivity');
Route::post('activity/save', 'ActivityController@saveActivity');
Route::get('activity/edit/{id}', 'ActivityController@editActivity');
Route::post('activity/update', 'ActivityController@updateActivity');
Route::get('activity/del/{id}', 'ActivityController@destroy');
Route::match(['get', 'post'], 'summary', 'ActivityController@showSummary');
Route::match(['get', 'post'], 'my_activity', 'ActivityController@viewMyActivity');
Route::get('summary/month/{month}/year/{year}/resource/{id}', 'ActivityController@showMonthly');


Route::match(['get', 'post'], 'sharing', 'ReportController@showSharing');

Route::get('entity/del/{id}', 'EntityController@destroy');
Route::resource('entity', 'EntityController');

Route::post('brand/filter', 'BrandController@filterBrand');
Route::get('brand/del/{id}', 'BrandController@destroy');
Route::resource('brand', 'BrandController');
Route::post('brand/ajax', 'BrandController@getBrands');

Route::match(['get', 'post'], 'resource/filter', 'ResourceController@filter');
Route::get('resource/del/{id}', 'ResourceController@destroy');
Route::resource('resource', 'ResourceController');

Route::get('status/del/{id}', 'StatusController@destroy');
Route::resource('status', 'StatusController');

Route::get('type/del/{id}', 'TypeController@destroy');
Route::resource('type', 'TypeController');

Route::resource('unit', 'BusinessUnitController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

Route::any('{all}', function(){
    return view('errors.404');
})->where('all', '.*');
