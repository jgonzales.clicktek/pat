<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\ProjectReference;

class ProjectReference extends Model {

	//
	public $timestamps = false;

	protected $table = 'm_project_references';

	public static function generateProjectRef($year = '')
	{
		if($year == '') {
			$year = date('Y');
		}

		$max = ProjectReference::where('year', $year)->max('project_number');

		$newProjNumber = ($max) ? $max + 1 : 1;
		$newProjectRefCode = $year."-".sprintf("%04d", $newProjNumber);

		$projRef = new ProjectReference;
		$projRef->year = $year;
		$projRef->project_number = $newProjNumber;
		$projRef->project_refcode = $newProjectRefCode;
		$projRef->save();

		return $newProjectRefCode;
	}

}
