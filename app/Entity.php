<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model {

	//
	public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 'm_entities';

	public static function filter($filter = [])
	{
		$query = Entity::orderBy('description');

		if(isset($filter['enabled'])) {
			$query->where('enabled', $filter['enabled']);
		}

		if(isset($filter['billable'])) {
			$query->where('billable', $filter['billable']);
		}

		// $entities = $query->get();

		return $query->get();
	}

}
