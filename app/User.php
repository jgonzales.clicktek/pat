<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'm_access';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public static function filter($filter = [])
	{
		$query = DB::table('m_access')
						->select('username', 'm_access.id', 'firstname', 'lastname', 'middlename', 
							'm_access.created_at', 'added_by', 'is_admin', 'm_access.enabled')
						->join('m_resources', 'm_resources.id', '=', 'm_access.resource_id')
						->orderBy('username');

		if(isset($filter['enabled'])) {
			$query->where('m_access.enabled', $filter['enabled']);
		}

		if(isset($filter['user_type'])) {
			$query->where('m_access.is_admin', $filter['user_type']);
		}

		// $entities = $query->get();

		return $query->get();
	}	

}
