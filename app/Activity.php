<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Activity extends Model {

	//
	public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 't_activities';

	public static function getAnnualSummary($filter)
	{
		$query = Activity::select('m_resources.id', 'firstname', 'lastname', 
						DB::raw('MONTH(date_log) as buwan'), 
						DB::raw('SUM(hours) as total_hours'))
					->join('m_resources', 't_activities.resource_id', '=', 'm_resources.id')
					->where('date_log', 'LIKE', '%'.$filter['year'].'%')
					->groupBy('m_resources.id')
					->groupBy('firstname')
					->groupBy('lastname')
					->groupBy('buwan')
					->orderBy('firstname')
					->orderBy('lastname');

		if (isset($filter['resource'])) {
			$query->where('is_internal', $filter['resource']);
		}

		if (isset($filter['business_unit'])) {
			$query->where('business_unit_id', $filter['business_unit']);
		}

		$result = $query->get();
					
		return $result;
	}

	public static function getAnnualBillableSummary($filter)
	{
		$query = Activity::select('m_resources.id', 'firstname', 'lastname', 
						DB::raw('MONTH(date_log) as buwan'), 
						DB::raw('SUM(hours) as total_hours'))
					->join('m_resources', 't_activities.resource_id', '=', 'm_resources.id')
					// ->join('m_brands', 't_activities.brand_id', '=', 'm_brands.id')
					->join('m_entities', 't_activities.entity_id', '=', 'm_entities.id')
					->where('date_log', 'LIKE', '%'.$filter['year'].'%')
					->where('m_entities.billable', 1)
					->where('t_activities.billable', 1)
					->groupBy('m_resources.id')
					->groupBy('firstname')
					->groupBy('lastname')
					->groupBy('buwan')
					->orderBy('firstname')
					->orderBy('lastname');

		if (isset($filter['resource'])) {
			$query->where('is_internal', $filter['resource']);
		}

		if (isset($filter['business_unit'])) {
			$query->where('business_unit_id', $filter['business_unit']);
		}

		$result = $query->get();
					
		return $result;
	}

	public static function getMonthlyActivity($year, $month, $id)
	{
		$result = Activity::select('m_resources.id', 'firstname', 'lastname','date_log',
						DB::raw('SUM(hours) as total_hours'))
					->join('m_resources', 't_activities.resource_id', '=', 'm_resources.id')
					->where('date_log', 'LIKE', $year.'-'.$month.'%')
					->where('resource_id', $id)
					->groupBy('m_resources.id')
					->groupBy('firstname')
					->groupBy('lastname')
					->groupBy('date_log')
					->orderBy('date_log')
					->get();

		return $result;
	}

	public static function getActivities($id, $filter = [])
	{
		$query = Activity::where('project_id', $id)
							->join('m_types', 'm_types.id', '=', 't_activities.type_id')
							->join('m_resources', 'm_resources.id', '=', 't_activities.resource_id')
							->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
							->orderByRaw('t_activities.date_log DESC,t_activities.id DESC')
							->select('t_activities.id', 'date_log','hours', 'activity', 'resource_id',
								 'm_resources.firstname','m_resources.lastname', 'm_resources.business_unit_id',
								DB::raw('(m_brands.description) as brand'),
								DB::raw('(m_types.description) as act_type')
							);
		// echo "<pre>";
		// print_r($filter);

		if (isset($filter['resource'])) {
			$query->where('resource_id', $filter['resource']);
		}

		if (isset($filter['business_unit'])) {
			$query->where('business_unit_id', $filter['business_unit']);
		}

		return $query->paginate(10);
	}

}
