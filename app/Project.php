<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Project extends Model {

	//
	// public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 't_projects';

	public static function filterProject($filter = [])
	{
		$query = DB::table('t_projects')
					->join('m_statuses', 'm_statuses.id', '=', 't_projects.status_id', 'LEFT')
					->join('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id', 'LEFT')
					->select('t_projects.*', DB::raw('m_statuses.description as status, m_ticket_types.name as ticket'));

		if(isset($filter['projmngr'])) {
			$query->where('project_manager_id', $filter['projmngr']);
		}

		if(isset($filter['resource'])) {
			$query->join('t_activities', 't_activities.project_id', '=', 't_projects.id')
						->where('resource_id', $filter['resource']);
		}

		if(isset($filter['my_recent_projects'])) {
			
			$date_range = explode("|", $filter['date_range']);

			$query->join('t_activities', 't_activities.project_id', '=', 't_projects.id')
						->where('resource_id', $filter['my_recent_projects'])
						->whereBetween('date_log', $date_range)
						->orderBy('date_log', 'DESC');
		}

		if(isset($filter['status'])) {
			$query->where('status_id', $filter['status']);
		}

		if(isset($filter['myprojects'])) {
			$query = DB::table('t_projects')
						-> join('m_statuses', 'm_statuses.id', '=', 't_projects.status_id', 'LEFT')
						->join('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id', 'LEFT')
						->select('t_projects.*', DB::raw('m_statuses.description as status, m_ticket_types.name as ticket'))
						->where('project_manager_id', $filter['myprojects'])
						->where('status_id', $filter['status'])
						->union($query);
		}

		$projects = $query->distinct()->orderBy('description')->get();


		return $projects;
	}

}
