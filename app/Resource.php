<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Resource;
use DB;

class Resource extends Model {

	//
	public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 'm_resources';

	public static function checkUniqueName($firstname, $lastname)
	{
		$resource = Resource::where('lastname', $lastname)->where('firstname', $firstname)->first();

		// echo "<pre>";
		// print_r($resource);

		return (count($resource) > 0);
	}

	public static function getResourceWithBelowMinLogs($date)
	{
		DB::enableQueryLog();

		$resource = DB::table('m_resources as r')
						->select('firstname', 'r.email', 'total_hrs')
						->join('m_access as u', 'u.resource_id', '=', 'r.id')
						->leftJoin(DB::raw("(SELECT resource_id,SUM(hours) AS total_hrs FROM t_activities 
									WHERE date_log = '$date'
									GROUP BY resource_id) a"),
							function($join) {
								$join->on('a.resource_id', '=', 'r.id');
							})
						->where('r.enabled', 1)
						->where(function($query) {
							$query->where('total_hrs', '<', env('MIN_DAILY_HRS_RENDERED'))
								->orWhereNull('total_hrs');
						})
					->get();

		// $query = DB::getQueryLog();
		// echo $query[0]['query'];
		// die();

		return $resource;

	}

	public static function filter($filter)
	{
		

		$query = DB::table('m_resources')->orderByRaw('lastname,firstname');

		if (isset($filter['business_unit'])) {
			$query->where('business_unit_id', $filter['business_unit']);
		}

		if (isset($filter['internal'])) {
			$query->where('is_internal', $filter['internal']);
		}

		if (isset($filter['enabled'])) {
			$query->where('enabled', $filter['enabled']);
		}

		return $query->get();
	}

}
