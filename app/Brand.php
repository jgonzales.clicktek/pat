<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Brand extends Model {

	//
	//protected $fillable = ['brandid','userid'];
	public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 'm_brands';

	public static function filterBrands($filter = array())
	{
		$query = Brand::select('m_brands.id', 'm_brands.enabled',
						DB::raw('m_entities.description as entity'),
						DB::raw('m_brands.description as brand'))
					->join('m_entities', 'm_entities.id', '=', 'm_brands.entity_id')
					->orderByRaw('entity,brand');

		if (isset($filter['entity'])) {
			$query = $query->where('entity_id', $filter['entity']);
		}

		if (isset($filter['enabled'])) {
			$query = $query->where('m_brands.enabled', $filter['enabled']);
		}

		$brands = $query->get();
		return $brands;
	}

	public static function getBrandByEntity($entity)
	{
		$brands = Brand::where('entity_id', $entity)->orderBy('description')->get();

		return $brands;

	}

}
