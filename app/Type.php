<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model {

	//
	public $timestamps = false;
	// protected $primaryKey = 'empno';
	protected $table = 'm_types';

	public static function filter($filter = [])
	{
		$query = Type::orderBy('description');

		if(isset($filter['enabled'])) {
			$query->where('enabled', $filter['enabled']);
		}
		// $entities = $query->get();

		return $query->get();
	}	

}
