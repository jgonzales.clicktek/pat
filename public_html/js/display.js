$(function(){
    $('#loader').hide();

    $("#brand").change(function() {
        var $select = $('#division');
        $select.find('option').remove();
        $('#department').find('option').remove();
        $('#class').find('option').remove();
    	$.ajax({
		  	method: "POST",
		  	url: '/ccri/asr/division',
		  	data: $('#frmASR').serializeArray(),
		  	success: function(response){
                var result = JSON.parse(response),options = '<option></option>';
                var val = '';
                for(var i=0; i < result.length; i++){
                    val = result[i].isdept;
                    options+='<option value="'+val+'">'+result[i].dptnam+' ['+val+']</option>';
                }
                $select.append(options);
		  	}
		});
    });

    $("#division").change(function() {
        var $select = $('#department');
        $select.find('option').remove();
        $('#class').find('option').remove();
        $.ajax({
            method: "POST",
            url: '/ccri/asr/department',
            data: $('#frmASR').serializeArray(),
            success: function(response){
                var result = JSON.parse(response),options = '<option></option>';
                var val = '';
                for(var i=0; i < result.length; i++){
                    val = result[i].iclas;
                    options+='<option value="'+val+'">'+result[i].dptnam+' ['+val+']</option>';
                }
                $select.append(options);
            }
        });
    });

    $("#department").change(function() {
        var $select = $('#class');
        $select.find('option').remove();
        $.ajax({
            method: "POST",
            url: '/ccri/asr/class',
            data: $('#frmASR').serializeArray(),
            success: function(response){
                var result = JSON.parse(response),options = '<option></option>';
                var val = '';
                for(var i=0; i < result.length; i++){
                    val = result[i].isclas;
                    options+='<option value="'+val+'">'+result[i].dptnam+' ['+val+']</option>';
                }
                $select.append(options);
            }
        });
    });

    function displayStocks() {
        $('#dataTables-example').html('');
        $.ajax({
            method: "POST",
            url: '/ccri/asr/display',
            data: $('#frmASR').serializeArray(),
            beforeSend: function() { 
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            }, 
                    //dataType: 'json',
            success: function(response){
                $('#dataTables-example').html(response);
            }
        });
    }

    function queryStocks() {
        $('#dataTables-example').html('');
        
        $.ajax({
            method: "POST",
            url: '/ccri/asr/display',
            data: $('#frmASR').serializeArray(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            }, 
                    //dataType: 'json',
            success: function(response){
                var result = JSON.parse(response);
                var table = '<table style="table-layout:fixed" class="table table-striped table-bordered table-hover" id="dataTables-example">';
                    table += '<thead>';
                    table += '<tr>';
                    table += '<th style="position: fixed;width: 100px">Style</th>';
                    table += '<th>SKU</th>';
                    table += '<th>Size</th>';                    
                    table += '<th>Description</th>';
                    table += '<th>Color</th>';
                var stores = result[0], data = result[1];
                    for(var i=0; i < stores.length; i++){
                        table += '<th>'+stores[i].desc+'</th>';
                    }
                    table += '</tr>';
                    table += '</thead>';
                    table += '<tbody>';
 
                var store, storeDesc = '';
                for(var i=0; i < data.length; i++){
                    table += '<tr>';
                    table += '<td style="position: fixed; width: 100px">'+data[i].style + '</td>';
                    table += '<td>'+data[i].upc + '</td>';
                    table += '<td>'+data[i].size + '</td>';
                    table += '<td>'+data[i].description + '</td>';
                    table += '<td>'+data[i].color + '</td>';
                    store = data[i].store;
                    var found=false, onhand;
                    for(var j=0; j < stores.length; j++){
                        found=false;
                        for(var x=0; x < store.length; x++) {
                            if(stores[j].no == store[x].storeno) {
                                found=true;
                                onhand = parseInt(store[x].onhand);
                                break;
                            }
                        }
                        onhand = (found==true) ? onhand : '0';
                        table += '<td>'+onhand+'</td>';
                    }
                    table += '</tr>';
                }
                table += '</tbody>';
                table += '</table>';
                $('#dataTables-example').html(table);
            }
        });
    }

    $("#displaystocks").click(function() {
        queryStocks();
    });
});