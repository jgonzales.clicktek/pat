$(function(){
    var tableHeader = '<table class="table table-striped table-bordered table-hover" id="dataTables-example">';
        tableHeader += '<thead>';
        tableHeader += '<tr>';
        tableHeader += '<th>Style</th>';
        tableHeader += '<th>UPC</th>';
        tableHeader += '<th>Size</th>';                    
        tableHeader += '<th>Description</th>';
        tableHeader += '<th>Item Type</th>';

    $('#loader').hide();

    $(".form-control").change(function() {
        var displayTable = tableHeader + '</table>';
        $('#dataTables-example').html(displayTable);
    });

    $("#brand").change(function() {
        var $select = $('#division');
        $select.find('option').remove();
        $('#department').find('option').remove();
        $('#class').find('option').remove();
    	$.ajax({
		  	method: "POST",
		  	url: '/ccri/asr/division',
		  	data: $('#frmASR').serializeArray(),
		  	success: function(response){
                var result = JSON.parse(response),options = '<option></option>';
                var val = '';
                for(var i=0; i < result.length; i++){
                    val = result[i].isdept;
                    options+='<option value="'+val+'">'+result[i].dptnam+' ['+val+']</option>';
                }
                $select.append(options);
		  	}
		});
    });

    $("#division").change(function() {
        var $select = $('#department');
        $select.find('option').remove();
        $('#class').find('option').remove();
        $.ajax({
            method: "POST",
            url: '/ccri/asr/department',
            data: $('#frmASR').serializeArray(),
            success: function(response){
                var result = JSON.parse(response),options = '<option></option>';
                var val = '';
                for(var i=0; i < result.length; i++){
                    val = result[i].iclas;
                    options+='<option value="'+val+'">'+result[i].dptnam+' ['+val+']</option>';
                }
                $select.append(options);
            }
        });
    });

    $("#department").change(function() {
        var $select = $('#class');
        $select.find('option').remove();
        $.ajax({
            method: "POST",
            url: '/ccri/asr/class',
            data: $('#frmASR').serializeArray(),
            success: function(response){
                var result = JSON.parse(response),options = '<option></option>';
                var val = '';
                for(var i=0; i < result.length; i++){
                    val = result[i].isclas;
                    options+='<option value="'+val+'">'+result[i].dptnam+' ['+val+']</option>';
                }
                $select.append(options);
            }
        });
    });

    function queryStocks() {
        $('#dataTables-example').html('');
        
        $.ajax({
            method: "POST",
            url: '/ccri/report/display',
            data: $('#frmASR').serializeArray(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            }, 
                    //dataType: 'json',
            success: function(response){
                var result = JSON.parse(response);
                var table = tableHeader;
                var stores = result[0], data = result[1], totals = result[2];
                var warehouseIndex = 0, TotalOnHand = 0;
                    for(var i=0; i < stores.length; i++){
                        table += '<th>'+stores[i].desc+'</th>';
                        if(stores[i].desc == 'On Hand')
                            warehouseIndex = i;
                    }
                    table += '</tr>';
                    table += '</thead>';
                    table += '<tbody>';
 
                var store, storeDesc = '';
                for(var i=0; i < data.length; i++){
                    table += '<tr>';
                    table += '<td>'+data[i].style + '</td>';
                    table += '<td>'+data[i].upc + '</td>';
                    table += '<td>'+data[i].size + '</td>';
                    table += '<td>'+data[i].description + '</td>';
                    table += '<td>'+data[i].itemtype + '</td>';
                    store = data[i].store;
                    var found=false, onhand;
                    for(var j=0; j < stores.length; j++){
                        found=false;
                        for(var x=0; x < store.length; x++) {
                            if(stores[j].no == store[x].storeno) {
                                found=true;
                                onhand = parseInt(store[x].onhand);
                                if(store[x].storeno == 7000)
                                    TotalOnHand += onhand;
                                break;
                            }
                        }
                        onhand = (found==true) ? parseInt(onhand) : '0';
                        table += '<td>'+onhand+'</td>';
                    }
                    table += '</tr>';
                }
                table += '<tr>';
                table += '<th colspan="5" align="right">TOTAL</th>';
                for(var j=0; j < stores.length; j++){
                    found=false;
                    $.each(totals, function(key, value) {
                        if(key == stores[j].no) {
                            onhand = parseInt(value);
                            found=true;
                            return false;   //break $.each iteration
                        }
                    });
                    onhand = (found==true) ? parseInt(onhand) : '0';
                    table += '<th>'+onhand+'</th>';
                }
                table += '</tr>';
                table += '</tbody>';
                table += '</table>';
                $('#dataTables-example').html(table);
                $('#total_on_hand').html(TotalOnHand);
                stickyMe();
            }
        });
    }

    $("#displaystocks").click(function() {
        queryStocks();
    });
});