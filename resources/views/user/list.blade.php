@extends('authenticated')

@section('content')

<div class="panel panel-info">
	<div class="panel-heading">
		List of Users
	</div>
	<div class="panel-body">
		@if (session('successmsg'))
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				{{ session('successmsg') }}
			</div>
		@endif
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form method="POST" class="form-inline well" action="{{ url('user/filter') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="container-fluid">
				<div class="row">
					<div class="form-group col-sm-3">
						<label for="user_type">User Type:</label>
						<select name="user_type" id="user_type" class="form-control">
							<option value="">[ All ]</option>
							@foreach($userType as $key => $val)
								<option value="{{ $key }}" @if(isset($filter['user_type']) && $filter['user_type'] == $key) selected="selected" @endif>{{$val}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-3">
						<label for="enabled">Status:</label>
						<select name="enabled" id="enabled" class="form-control">
							<option value="">[ All ]</option>
							@foreach($options as $key => $val)
								<option value="{{ $key }}" @if(isset($filter['enabled']) && $filter['enabled'] == $key) selected="selected" @endif>{{$val}}</option>
							@endforeach 
						</select>
					</div>
					<div class="col-xs-6 col-md-4 buttons">
						<label>&nbsp;</label>
						<button type="submit" name="search" class="btn btn-primary">
							<i class="fa fa-search-plus"></i> Filter
						</button>
					</div>
				</div>
				<br />
			</div>
		</form>
		<form method="POST" class="form-inline" id="employeelist">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<tr>
						<th>Username</th>
						<th>First Name</th>
						<th>Middle Name</th>
						<th>Last Name</th>
						<th>Date Created</th>
						<th>Added By</th>
						<th>User Type</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					@foreach($users as $user)
					<tr>
						<td>{{ $user->username }}</td>
						<td>{{ $user->firstname }}</td>
						<td>{{ $user->middlename }}</td>
						<td>{{ $user->lastname }}</td>
						<td>{{ $user->created_at }}</td>
						<td>{{ $user->added_by }}</td>
						<td>{{ $user->is_admin ? "Admin" : "Regular" }}</td>
						<td>{{ $user->enabled ? "Active" : "Access Removed" }}</td>
						<td nowrap="">
						@if($user->enabled)
							<a class="btn btn-success fa fa-key" href="{{ url('user/set-active/0/') }}/{{$user->id}}" title="Click to Remove Access" onclick="return confirm('This action will remove access to the user.\n\nAre you sure you want to proceed?');">
							</a>
						@else
							<a class="btn btn-danger fa fa-lock" href="{{ url('user/set-active/1/') }}/{{$user->id}}" title="Click to Grant Access" onclick="return confirm('This action will grant access to the user.\n\nAre you sure you want to proceed?');">
							</a>
						@endif
						<a class="btn btn-primary fa fa-edit" href="{{ url('user/edit') }}/{{$user->id}}" title="Edit User"></a>
						<a class="btn btn-danger fa fa-recycle" href="{{ url('user/reset-password') }}/{{$user->id}}" title="Reset Password" onclick="return confirm('Are you sure you want to proceed?');"></a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>
			<div>
				<?php 
				/*
				if($pages > 1) {
					echo '<ul class="pagination">';
						for($i=1; $i<=$pages; $i++) {
							$active = ($i==1) ? ' class="active"' : "";
							echo '<li'.$active.'><a href="#">'.$i.'</a></li>';
						}
					echo '</ul>';
				}*/
				?>
			</div>
		</form>
	</div>
	<div class="panel-footer">
	</div>
</div>

@endsection