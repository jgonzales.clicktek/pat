@extends('authenticated')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Edit User</div>
				<div class="panel-body">
					@if (isset($successmsg))
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							{{ $successmsg }}
						</div>
					@endif
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/update') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $user->id }}">

						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">Username</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="username" placeholder="Username" value="{{ $user->username }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">First Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="firstname" placeholder="First Name" value="{{ $user->firstname or '' }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">Middle Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="middlename" placeholder="Middle Name" value="{{ $user->middlename or '' }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">Last Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="lastname" placeholder="Last Name" value="{{ $user->lastname or '' }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">User Type</label>
							<div class="col-md-5">
								<select name="usertype" class="form-control">
								@foreach($types as $key => $val)
									<option value="{{ $key }}" {{ $key==$user->is_admin ? "selected" : "" }}>{{ $val }}</option>
								@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Save
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
