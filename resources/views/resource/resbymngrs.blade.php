@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="panel panel-info">
	<div class="panel-heading">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3" style="margin-left: -10px"><h5><strong>Resources by Manager</strong></h5></div>
				<div class="col-sm-3 col-sm-offset-6 right-align">
                    <a right-align type="button" href="{{ url('resource') }}" class="btn btn-default">
                        <i class="fa fa-close"></i> Cancel
                    </a>
				</div>
			</div>
		</div>		
	</div>

	<!-- @if(isset($resource)) -->
        @foreach($resource as $res)
			<div class="row">
				<div class="col-md-12">
					<table class="table">
						<thead>
						</thead>
						<tbody>
							<tr>
								<td class="col-md-4" style="border-top:hidden; padding-left:100px;">
									<b>{!! $res['unit'] !!}</b>
								</td>
							</tr>
							@if(isset($res['manager']))
								<tr style="border:hidden;">
									<td class="col-md-6" style="padding-left:150px;">
										{!! $res['manager'] !!}
									</td>
								</tr>
							@endif
							@if(isset($res['TLsubordinates']))
								@foreach($res['TLsubordinates'] as $ts)
									<tr>
										<td class="col-md-6" style="padding-left:200px; border-bottom:hidden;">
											{!! $ts['subordinates'] !!}
										</td>
									</tr>
									<div class="col-md-6">
										@foreach($ts['TLsub'] as $tlSub)

											<tr>
												<td class="col-md-6" style="padding-left:250px; border-bottom:hidden;">
													{!! $tlSub->name !!}
												</td>
											</tr>
										@endforeach
									</div>
								@endforeach
							@endif
						</tbody>
					</table>
				</div>
			</div>
		@endforeach
	<!-- @endif -->
</div>
@endsection