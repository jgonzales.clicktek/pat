@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="panel panel-info">
	<div class="panel-heading">
		Edit Resource
	</div>
	<div class="panel-body">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form method="POST" class="form-inline well" action="{{url('resource')}}/{{$resource->id}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input name="_method" type="hidden" value="PATCH">
			<input name="id" type="hidden" value="{{ $resource->id }}">
			<div class="container-fluid">
				<div class="row">
					<div class="form-group col-sm-3">
						<label for="firstname">First Name:</label>
						<input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" required="" value="{{ $resource->firstname }}">
					</div>
					<div class="form-group col-sm-3">
						<label for="middlename">Middle Name:</label>
						<input type="text" name="middlename" id="middlename" class="form-control" placeholder="Middle Name" value="{{ $resource->middlename }}">
					</div>
					<div class="form-group col-sm-3">
						<label for="lastname">Lastname:</label>
						<input type="text" name="lastname" id="lastname" class="form-control" placeholder="Lastname" required="" value="{{ $resource->lastname }}">
					</div>
					<div class="form-group col-sm-3">
						<label for="business_unit">Business Unit:</label>
						<select class="form-control" name="business_unit" id="business_unit">
							<option value="">[Select Business Unit]</option>
							@foreach ($units as $unit)
								<option value="{{ $unit->id }}" @if ($unit->id == $resource->business_unit_id)  selected="selected" @endif >{{ $unit->description }} </option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-sm-3">
						<label for="email">Email:</label>
						<input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ $resource->email }}">
					</div>
					<div class="form-group col-sm-3">
						<label for="start_date">Start Date:</label>
						<input type="text" name="start_date" id="start_date" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $resource->start_date }}">
					</div>
					<div class="form-group col-sm-3">
						<label for="end_date">End Date:</label>
						<input type="text" name="end_date" id="end_date" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $resource->end_date }}">
					</div>
					<div class="form-group col-sm-3">
						<label for="manager">Manager:</label>
						<select class="form-control" name="manager" id="manager">
							<option value="">[Select Manager/Lead]</option>
							@foreach ($managers as $manager)
								<option value="{{ $manager->id }}" @if ($manager->id == $resource->manager_id)  selected="selected" @endif >{{ $manager->firstname }} {{ $manager->lastname }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-sm-3">
						{{-- <label></label> --}}
						<br />
						<div class="checkbox form-group">
							<label><input type="checkbox" value="1" name="enabled" @if($resource->enabled) checked="" @endif> <b>Enabled</b></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-sm-3">
						{{-- <label></label> --}}
						<br />
						<div class="checkbox form-group">
							<label><input type="checkbox" value="1" name="internal" @if($resource->is_internal) checked="" @endif> <b>Internal Resource</b></label>
						</div>
					</div>
				</div>
				<div class="row">
					<br />
					<div class="col-xs-6 col-md-4 buttons">
						<button type="submit" name="action" class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
						<a type="button" href="{{ url('resource') }}" class="btn btn-default">
							<i class="fa fa-close"></i> Cancel
						</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});
	</script>
@endsection