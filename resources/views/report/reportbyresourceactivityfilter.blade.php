@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	<form method="POST" class="form-inline well">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		{{-- <input type="hidden" name="project_id" value="{{ $project->id }}"> --}}
        <p align="left" class="alert alert-info">{{ config('app.rptR_msg') }}</p>
		<div class="container-fluid">
			<div class="row">
				{{-- @if(Auth::user()->is_admin) --}}
					<div class="form-group col-sm-3">
						<label for="description">Resource:</label>
						<select name="resource" id="resource" class="form-control"> 
							<option value="">-- All --</option>
							@foreach($resources as $res)
								<option value="{{ $res->id }}" @if($filter['resource'] == $res->id) selected="selected" @endif>{{$res->firstname}} {{$res->lastname  }}</option>
							@endforeach
						</select>
                        <input type="hidden" value="" name="h_resource" id="h_resource">
                        <input type="hidden" name="h_page_no" id="h_page_no">
					</div>
				{{-- @endif --}}
				<div class="form-group col-sm-3">
					<label for="entity">Entity:</label>
					<select name="entity" id="entity" class="form-control" required="">
						<!--option value="">Select Entity</option-->
                                                <option value="0">-- All --</option>
						@foreach($entities as $entity)
							<option value="{{ $entity->id }}" @if($entity->id == $filter['entity']) selected="selected" @endif>{{ $entity->description }}</option>
						@endforeach
					</select>
                    <input type="hidden" value="" name="h_entity" id="h_entity">
				</div>
				<div class="form-group col-sm-3">
					<label for="brand">Brand:</label>
					<select name="brand" id="brand" class="form-control" required="">
						<option value="">Select Brand</option>
						@foreach($brands as $brand)
							<option value="{{ $brand->id }}" @if($brand->id == $filter['brand']) selected="selected" @endif>{{ $brand->description }}</option>
						@endforeach
					</select>
                    <input type="hidden" value="" name="h_brand" id="h_brand">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="fromDate">Start:</label>
					<input type="text" name="fromDate" id="fromDate" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $filter['from'] }}">
                    <input type="hidden" value="" name="h_fromDate" id="h_fromDate">
				</div>
				<div class="form-group col-sm-3">
					<label for="end">End:</label>
					<input type="text" name="toDate" id="toDate" class="form-control datepicker" placeholder="yyyy-mm-dd"  value="{{ $filter['to'] }}">
                    <input type="hidden" value="" name="h_toDate" id="h_toDate">
				</div>
				<div class="col-sm-5 buttons">
					<label>&nbsp;</label>
					<button type="button" name="search" id="search" class="btn btn-primary" value="search">
						<i class="fa fa-search-plus"></i> Search
					</button>
					<button type="submit" name="submit" class="btn btn-primary" value="download">
						<i class="fa fa-download"></i> Download
					</button>
				</div>
				<br />
			</div>
			<br />
		</div>
	</form>
    <div id="output">
        @include ('report.reportbyresourceactivity');
    </div>
@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
        $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});
        $(function() {
            var brands;
            var selectedBrands;
            var prevBrand = {{ $filter['brand'] or '-1' }};

            $.ajax({
                url: '{{ url('brand/ajax') }}',
                type: 'POST',
                data: { _token: "{{ csrf_token() }}"},
                dataType: 'JSON',
                success: function (data) {
                    console.log(data);
                    brands = data;
                }
            });

            function get()
            {
                selectedBrands = [];
                for (var i = 0; i < brands.length; i++) {
                    if ($("#entity").val() == brands[i].entity_id) 
                    {
                        selectedBrands.push(brands[i]);
                    }
                }

                $("#brand").empty().append('<option value=""></option>');
                var select = '';
                for (var i = 0; i < selectedBrands.length; i++) 
                {
                    if (selectedBrands[i].id == prevBrand) 
                    {
                        select = 'selected="selected"';
                    }
                    $("#brand").append('<option value="' + selectedBrands[i].id + ' '+select + '">' + selectedBrands[i].description +'</option>');
                }

                if (selectedBrands.length == 1) {
                    $("#brand")[0].selectedIndex = 1;
                }

                document.getElementById("brand").selectedIndex = prevBrand;

            }

            $("#entity").change(function() 
            {
                selectedBrands = [];
                for (var i = 0; i < brands.length; i++) 
                {
                    if ($(this).val() == brands[i].entity_id) 
                    {
                        selectedBrands.push(brands[i]);
                    }
                }

                $("#brand").empty().append('<option value=""></option>');
                for (var i = 0; i < selectedBrands.length; i++) 
                {
                    $("#brand").append('<option value="' + selectedBrands[i].id + '">' + selectedBrands[i].description +'</option>');
                }

                if (selectedBrands.length == 1)
                {
                    $("#brand")[0].selectedIndex = 1;
                }
                if ($("#entity option:selected").text() == 'ADMIN') 
                {
                    $("#billable").removeAttr('checked').attr('disabled', 'true');
                        selectedBrands = [296,297,298,302,312,319];
                    $("#brand").append('<option value="' + selectedBrands + '">--All--</option>');
                } else {
                    $("#billable").prop('checked', true).removeAttr('disabled');
                }
            });
        });

        $(document).ready(function(){
            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                fetch_data(page);
            });

            function fetch_data(page)
            {
                var resource = '&resource=' + $('#h_resource').val();
                var entity = '&entity=' + $('#h_entity').val();
                var brand = '&brand=' + $('#h_brand').val();
                var fromDate = '&fromDate=' + $('#h_fromDate').val();
                var toDate = '&toDate=' + $('#h_toDate').val();

                $.ajax({
                    url:"/report_by_resource_activity/fetch_data?page="+page+resource+entity+brand+fromDate+toDate,
                    success:function(data)
                    {
                        $('#output').html(data);
                    }
                });
            }

            $('#search').click(function(){
                var resource = $('#resource').val();
                var entity = $('#entity').val();
                var brand = $('#brand').val();
                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                $('#h_resource').val(resource);
                $('#h_entity').val(entity);
                $('#h_brand').val(brand);
                $('#h_fromDate').val(fromDate);
                $('#h_toDate').val(toDate);

                fetch_data(1);
            });
        });

        $("#entity").change(function() {
            if ($(this).val() == 0) {
                $("#brand").attr("disabled", "disabled");
            } else {
                $("#brand").removeAttr("disabled");
            }
        }).trigger("change");	
    </script>
@endsection