<div align="right">
<?php echo $activities->render(); ?>
</div>

<div class="panel panel-info">
	<div class="panel-heading">
		Activities
	</div>
	<div class="panel-body">
		<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
			<table class="table table-striped table-bordered table-hover" id="dataTable">
				<tr>
					<th>#</th>
					<th>Ticket Reference</th>
					<th>Ticket</th>
					<th>Resource</th>
					<th>Status</th>
					<th>Entity</th> 
					<th>Brand</th>
					<th>Date Log</th>
					<th>Type</th>
					<th>Hours</th>
				</tr>
				<?php $i=1 ?>
				<?php $totalHours = 0 ?>
				@forelse($activities as $activity)
					<tr>
						<td>{{ $i++ }}.</td>
						<td>{{ $activity->project_ref }}</td>
						<td><a href="{{ url('project/details')}}/{{$activity->project_id}}">{{ $activity->project }} </a> </td>
						<td>{{ $activity->lastname }}</td>
						<td>{{ $activity->status }}</td>
						<td>{{ $activity->entity }}</td>
						<td>{{ $activity->brand }}</td>
						<td nowrap="">{{ $activity->date_log }}</td>
						<td>{{ $activity->ticket_type }}</td>
						<td class="right-align">{{ number_format($activity->hours, 2) }}</td>
						<?php $totalHours += $activity->hours; ?>
					</tr>
					@empty
						<tr>
							<td colspan="10">
								<div class="alert alert-info">
									No data found.
								</div> 
							</td>
						</tr>
					@endforelse
				<!-- <tr>
					<td colspan="10" class="right-align">TOTAL HOURS </td>
					<td class="right-align">{{ number_format($totalHours, 2) }}</td>
					<td></td>
				</tr> -->
			</table>
		</div>
	</div>
</div>
