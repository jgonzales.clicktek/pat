@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="description">Resource:</label>
					<select name="resource" id="resource" class="form-control">
						<option value="">Select Resource</option>
						@foreach($resources as $resource)
							<option value="{{ $resource->id }}" @if($defaultResource == $resource->id) selected="selected" @endif>{{$resource->firstname}} {{$resource->lastname  }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-2">
					<label for="fromDate">Date From:</label>
					<input type="text" name="fromDate" id="fromDate" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $fromDate }}">
				</div>
				<div class="form-group col-sm-2">
					<label for="end">Date To:</label>
					<input type="text" name="toDate" id="toDate" class="form-control datepicker" placeholder="yyyy-mm-dd"  value="{{ $toDate }}">
				</div>
				<div class="form-group col-sm-2">
					<label for="resource_type">Resource Type:</label>
					<select name="resource_type" id="resource_type" class="form-control">
						<option value="-1">All</option>
						@foreach($resourceTypes as $key => $type)
							<option value="{{ $key }}" @if($key === $selectedResource) selected="selected" @endif >{{$type }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-2">
					{{-- <label>&nbsp;</label> --}}
					<br />
					<label><input type="checkbox" name="includeAdmin" @if ($includeAdmin) checked="" @endif> Include Admin</label>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3 buttons">
					<label>&nbsp;</label>
					<button type="submit" name="search" class="btn btn-primary">
						<i class="fa fa-search-plus"></i> Search
					</button>
				</div>
			</div>
			<br />
		</div>
	</form>
	<div class="panel panel-info">
		<div class="panel-heading">Entity Sharing</div>
		<div class="panel-body">
			<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
				<table class="table table-bordered" id="dataTable">
					<tr>
						<th>Resource</th>
						@foreach($entities as $entity) 
							<th>{{ $entity->description }}</th>
						@endforeach
						<th>Total</th>
					</tr>

					@foreach($summary as $resource)
						<tr>
							<td rowspan="2">{{ $resource['name'] }}</td>
							<?php $totalHours = 0; ?>
							@foreach($entities as $entity) 
								<td class="center">
								<?php
									if(isset($resource['company'][$entity->description])) {
										$hours = $resource['company'][$entity->description]['hours'];
										echo $hours;
										$totalHours += $hours;
									}
								?>
								</td>
							@endforeach
							<td class="center">{{ $totalHours }}</td>
						</tr>
						<tr>
							<?php $totalPercent = 0; ?>
							@foreach($entities as $entity) 
								<td class="center">
								<?php
									if(isset($resource['company'][$entity->description])) {
										$percent = $resource['company'][$entity->description]['percent'];
										// echo (strpos($percent, '.')) ? number_format($percent,2).'%' : $percent . '%';
										echo number_format($percent,4)."%"; 
										$totalPercent += $percent;
									}
								?>
								</td>
							@endforeach
							<td class="center">{{ $totalPercent }}%</td>
						</tr>
					@endforeach
				</table>
			</div>
		
		</div>
		
	</div>
@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});
	</script>
@endsection