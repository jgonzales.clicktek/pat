@extends('authenticated')

@section('additionalstylesheets')
    {{--<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <style>
        .multiselect {
            position: relative;
            min-height: 34px;
        }

        .multiselect-container {
            background-color: #eeeeee;
        }

        .form-inline .multiselect-container span.form-check .form-check {
            display: block !important;
            padding: 0 !important;
        }

        .multiselect-container .form-check-label {
            font-weight: normal;
            display: contents;
            line-height: normal;
            padding-left: 5px;
        }

        input[type="checkbox"] {
            margin: 10px 0 0 0;
            line-height: normal;
        }

        .open > .dropdown-menu {
            display: inherit;
            text-align: left;
        }

        .multiselect-container .multiselect-option .multiselect-group-option-indented {
            padding-left: 3rem;
        }
    </style>
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" class="form-inline well">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        {{-- <input type="hidden" name="project_id" value="{{ $project->id }}"> --}}
        <p align="left" class="alert alert-info">{{ config('app.rptTAL_msg') }}</p>
        <div class="container-fluid">
            <div class="row">
                {{-- @if(Auth::user()->is_admin) --}}
                {{--    <div class="form-group col-sm-3">
                        <label for="description">Ticket Manager:</label>
                        <select id="resource" name="resource[]" class="form-control" multiple="multiple" required=""
                                style="width:auto;">
                            @foreach($resources as $res)
                                <optgroup label="{{$res['units']}}">
                                    @if(isset($res['details']))
                                        @foreach($res['details'] as $det)
                                            <option value="{{ $det->id }}">
                                                {{$det->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </optgroup>
                            @endforeach
                        </select>
                        <input type="hidden" value="" name="h_resource" id="h_resource">
                        <input type="hidden" name="h_page_no" id="h_page_no">
                    </div>--}}
                <div class="form-group col-sm-3">
                    <label for="description">Business Unit:</label>
                    <select id="business_unit" name="business_unit[]" class="form-control" multiple required="" style="width:auto;">
                        @foreach($business_units as $business_unit)
                            <option value="{{ $business_unit->id }}">
                                {{$business_unit->description}}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-sm-3">
                    <label for="description">Resources:</label>
                    <select id="resource" name="resource[]" class="form-control" multiple required="" style="width:auto;">
                        @foreach($business_units as $business_unit)
                            <option value="{{ $business_unit->id }}">
                                {{$business_unit->description}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


            {{-- @endif --}}
            <div class="row">
                <div class="form-group col-sm-3" >
                    <label for="fromDate">Start:</label>
                    <input type="text" name="fromDate" id="fromDate" class="form-control datepicker"
                           placeholder="yyyy-mm-dd" value="{{ $filterdate['from'] }}">
                    <input type="hidden" value="" name="h_fromDate" id="h_fromDate">
                </div>
                <div class="form-group col-sm-3">
                    <label for="end">End:</label>
                    <input type="text" name="toDate" id="toDate" class="form-control datepicker"
                           placeholder="yyyy-mm-dd" value="{{ $filterdate['to'] }}">
                    <input type="hidden" value="" name="h_toDate" id="h_toDate">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5 buttons">
                    <label>&nbsp;</label>
                    <button type="button" name="search" id="search" class="btn btn-primary" value="search">
                        <i class="fa fa-search-plus"></i> Search
                    </button>
                    <button type="submit" name="submit" id="download" class="btn btn-primary" value="download">
                        <i class="fa fa-download"></i> Download
                    </button>
                </div>
                <br/>
            </div>

            <br/>
        </div>
    </form>
    <div id="output">
        @include ('report.reportbyProjectActLog');
    </div>
@endsection

@section('customjs')
    <script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>--}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>


    <script type="text/javascript">
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});

        $(document).ready(function () {

            $('#business_unit').multiselect({
                nonSelectedText: 'Business Unit',
                buttonWidth: '230px',
                onChange: function (option, checked) {
                    $('#resource').html('');
                    $('#resource').multiselect('rebuild');
                    var selected = this.$select.val();
                    if (selected.length > 0) {
                        console.log(selected)
                        $.ajax({
                            url: "ticket_activity_log/fetch_ticket_owner",
                            method: "GET",
                            data: {selected: selected},
                            success: function (data) {
                                var test = [];
                                $.each(data, function(i, val) {
                                    test.push("<option value="+val.id+">"+val.firstname+" "+val.lastname+"</option")
                                });

                                $('#resource').html(test);
                                $('#resource').multiselect('rebuild');
                            }
                        })
                    }
                }
            });

            $('#resource').multiselect({
                nonSelectedText: 'Resource',
                buttonWidth:'230px',
                onChange:function(option, checked)
                {
                    var selected = this.$select.val();
                    console.log(selected)
                    //dito ka gawa ajax para sa resource id
                }
            });


            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                fetch_data(page);
            });

            function fetch_data(page) {
                var resource = '&resource=' + $('#h_resource').val();
                var fromDate = '&fromDate=' + $('#h_fromDate').val();
                var toDate = '&toDate=' + $('#h_toDate').val();
                console.log($('#h_resource').val());
                $.ajax({
                    url: "/ticket_activity_log/fetch_data?page=" + page + resource + fromDate + toDate,
                    success: function (data) {
                        $('#output').html(data);
                    }
                });
            }

            $('#search').click(function () {
                var resource = $('#resource').val();
                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                $('#h_resource').val(resource);
                $('#h_fromDate').val(fromDate);
                $('#h_toDate').val(toDate);

                fetch_data(1);
            });
        });

        $(document).ready(function () {
            $('#resource').multiselect({
                enableClickableOptGroups: true,
                buttonWidth: 250,
                nheritClass: true,
                maxHeight: 300,
                nonSelectedText: 'Select Ticket Manager',
            });
        });
    </script>
@endsection