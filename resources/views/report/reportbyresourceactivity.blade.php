<div align="right">
<?php echo $activities->render(); ?>
</div>

<div class="panel panel-info">
	<div class="panel-heading">
		Activities
	</div>
	<div class="panel-body">
		<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
			<table class="table table-striped table-bordered table-hover" id="dataTable">
				<tr>
					<th>#</th>
					<th>Date Log</th>
					<th>Ticket Reference</th>
					<th>Ticket Name</th>
					<th>Resource</th>
					<th>Activity</th>
					<th>Ticket Type</th>
					<th>Entity</th> 
					<th>Brand</th>
					<th>Task type</th>
					<th>Status</th>
					<th>Billable</th>
					<th>Hours</th>
				</tr>
				<?php $i=1 ?>
				<?php $totalHours = 0 ?>
					{{dd($activities)}}
				@forelse($activities as $activity)
					<tr>
						<td>{{ $i++ }}.</td>
						<td nowrap="">{{ $activity->date_log }}</td>
						<td>{{ $activity->project_ref }}</td>
						<td><a href="{{ url('project/details')}}/{{$activity->project_id}}">{{ $activity->project }} </a> </td>
						<td>{{ $activity->resource }}</td>
						<td>{{ $activity->activity }}</td>
						<td>{{ $activity->ticket_type }}</td>
						<td>{{ $activity->task_type }}</td>
						<td>{{ $activity->entity }}</td>
						<td>{{ $activity->brand }}</td>
						<td>{{ $activity->status }}</td>
                        <td>@if($activity->billable == 1) Yes @else No @endif </td>
						<td class="right-align">{{ number_format($activity->hours, 2) }}</td>
						<?php $totalHours += $activity->hours; ?>
					</tr>
					@empty
						<tr>
							<td colspan="12">
								<div class="alert alert-info">
									No data found.
								</div> 
							</td>
						</tr>
					@endforelse
				<!-- <tr>
					<td colspan="12" class="right-align">TOTAL HOURS </td>
					<td class="right-align">{{ number_format($totalHours, 2) }}</td>
					<td></td>
				</tr> -->
			</table>
		</div>
	</div>
</div>
