<div align="right">
<?php echo $activities->render(); ?>
</div>

<div class="panel panel-info">
	<div class="panel-heading">
		Activities
	</div>
	<div class="panel-body">
		<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
			<table class="table table-striped table-bordered table-hover" id="dataTable">
				<tr>
					<th>#</th>
					<th>Date Log</th>
					<th>Ticket Name</th>
					<th>Ticket Type</th>
					<th>Status</th>
					<th>Ticket Manager</th>
					<th>Resource</th>
					<th>Task Type</th> 
					<th>Entity</th>
					<th>Brand</th>
					<th>Hours</th>
				</tr>
				<?php $i=1 ?>
				<?php $totalHours = 0 ?>
					@forelse($activities as $activity)
					{{dd($activities)}}
						<tr>
							<td>{{ $i++ }}.</td>
							<td nowrap="">{{ $activity->date_log }}</td>
							<td><a href="{{ url('project/details')}}/{{$activity->project_id}}">{{ $activity->project }} </a> </td>
							<td>{{ $activity->ticket_type }}</td>
							<td>{{ $activity->status }}</td>
							<td>{{ $activity->ticket_manager }}</td>
							<td>{{ $activity->resource }}</td>
							<td>{{ $activity->task_type }}</td>
							<td>{{ $activity->entity }}</td>
							<td>{{ $activity->brand }}</td>
							<td class="right-align">{{ number_format($activity->hours, 2) }}</td>
							<?php $totalHours += $activity->hours; ?>
						</tr>
					@empty
						<tr>
							<td colspan="11">
								<div class="alert alert-info">
									No data found.
								</div> 
							</td>
						</tr>
					@endforelse
				<!-- <tr>
					<td colspan="12" class="right-align">TOTAL HOURS </td>
					<td class="right-align">{{ number_format($totalHours, 2) }}</td>
					<td></td>
				</tr> -->
			</table>
		</div>
	</div>
</div>
