<?php
    use \koolreport\widgets\koolphp\Table;
?>
<div class="report-content">
    <div class="text-center">
        <h1>Reprot by PM Ticket</h1>
    </div>

    <?php
    Table::create(array(
        "dataSource"=>DB::table('t_activities')
                    ->select('t_activities.id','hours', 'activity', 'date_log', 'project_id', 'project_ref', 'm_resources.firstname', 'm_resources.lastname',
                        DB::raw('(t_projects.description) as project'),
                        DB::raw('(m_entities.description) as entity'),
                        DB::raw('(m_brands.description) as brand'),
                        DB::raw('(m_statuses.description) as status'),
                        DB::raw('m_ticket_types.name AS ticket_type'))
                    ->leftJoin('t_projects', 't_projects.id', '=', 't_activities.project_id')
                    ->leftJoin('m_resources', 'm_resources.id', '=', 't_activities.resource_id') 
                    ->leftJoin('m_entities', 'm_entities.id', '=', 't_activities.entity_id')
                    ->leftJoin('m_brands', 'm_brands.id', '=', 't_activities.brand_id')
                    ->leftJoin('m_statuses', 'm_statuses.id', '=', 't_projects.status_id')
                    ->leftJoin('m_ticket_types', 'm_ticket_types.id', '=', 't_projects.ticket_type_id')
                    ->orderByRaw('t_activities.date_log DESC,t_activities.id DESC')
                    ->get()
    ));
    ?>
</div>