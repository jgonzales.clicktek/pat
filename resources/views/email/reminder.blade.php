<body>
<p>Hi {{$info->employee}},</p>

<p>A pleasant day to you!</p>

@if ($info->total_hrs == null)
	<p>The system has detected that you forgot to log anything on <a href="http://pat.rgoc.com.ph">PAT</a> today.</p> 
@elseif ($info->total_hrs < env('MIN_DAILY_HRS_RENDERED'))
	<p>The system has detected that you have less than {{ env('MIN_DAILY_HRS_RENDERED') }} hours on your <a href="http://pat.rgoc.com.ph">PAT</a> today.</p>
@endif

<p>A gentle reminder from <a href="http://pat.rgoc.com.ph">PAT</a> system.</p>

<p>Regards,<br />
PAT System</p>
</body>