@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<h4>Ticket Name: {{ $project->description }}</h4>
	<form method="POST" class="form-inline well" action="{{ url('activity/save') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="project_id" value="{{ $project->id }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3 col-sm-offset-1">
					<label for="entity">Entity:</label>
					<select name="entity" id="entity" class="form-control" required="">
						<option value="">Select Entity</option>
						@foreach($entities as $entity)
							<option value="{{ $entity->id }}" @if($entity->id == old('entity')) selected="selected" @endif>{{ $entity->description }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="brand">Brand:</label>
					<select name="brand" id="brand" class="form-control" required="">
						<option value="">Select Brand</option>
						{{-- @foreach($brands as $brand)
							<option value="{{ $brand->id }}" @if($brand->id == old('brand')) selected="selected" @endif>{{ $brand->description }}</option>
						@endforeach --}}
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="resource">Resource:</label>
					<select name="resource" id="resource" class="form-control" required="">
						<option value="">Select Resource</option>
						@foreach($resources as $resource)
							<option value="{{ $resource->id }}" @if($resource->id == Auth::user()->resource_id) selected="selected" @endif>{{$resource->firstname}} {{$resource->lastname}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3 col-sm-offset-1">
					
					<label for="date_log">Date:</label>
					<input type="text" name="date_log" id="date_log" class="form-control datepicker" placeholder="yyyy-mm-dd" required="" value="{{ old('date_log', date('Y-m-d') ) }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="type">Type:</label>
					<select name="type" id="type" class="form-control"  required="">
						<option value="">Select Type</option>
						@foreach($types as $type)
							<option value="{{ $type->id }}" @if($type->id == old('type')) selected="selected" @endif>{{ $type->description }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-10 col-sm-offset-1">
					<label for="activity">Activity:</label>
					<textarea class="form-control" name="activity" id="activity" rows="2">{{old('activity')}}</textarea>
				</div>
			</div>
			<div class="row">
				
			</div>
			<div class="row">
				<div class="form-group col-sm-3 col-sm-offset-1">
					<label for="hours">Hours:</label>
					<input type="text" name="hours" id="hours" class="form-control" placeholder="Hours" required="" value="{{ old('hours') }}">
				</div>
				<div class="form-group col-sm-8">
					{{-- <label></label> --}}
					<br />
					<div class="checkbox form-group">
						<label><input type="checkbox" value="1" name="billable" id="billable" checked=""> <b>Billable</b></label>
					</div>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-xs-6 col-md-4 col-sm-offset-1 buttons">
					<button type="submit" name="action" class="btn btn-primary">
						<i class="fa fa-save"></i> Save
					</button>
					<a type="button" href="{{ url($cancelUrl) }}" class="btn btn-default">
						<i class="fa fa-close"></i> Cancel
					</a>
				</div>
			</div>
		</div>
	</form>

@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});


		$(function() {

			// $("#entity").change(function() {
	  //       	$('#brand').find('option').remove();

			// 	$.ajax({
			// 	  	method: "POST",
			// 	  	url: '{{ url('brand/ajax') }}',
			// 	  	data: { entity: $('#entity').val(), "_token": "{{ csrf_token() }}"  },
			// 	  	success: function(response){
		 //                var result = JSON.parse(response), options = '<option></option>';
		 //                for (var i=0; i < result.length; i++) {
		 //                    options+='<option value="'+result[i].id+'">'+result[i].description+'</option>';
		 //                }
		 //                $("#brand").append(options);
		 //                if (result.length == 1) {
		 //                	$("#brand")[0].selectedIndex = 1;
		 //                }
			// 	  	}
			// 	});

			// });

			var brands;
			var selectedBrands;

			$.ajax({
	        	url: '{{ url('brand/ajax') }}',
	        	type: 'POST',
	        	data: { _token: "{{ csrf_token() }}"},
	        	dataType: 'JSON',
	        	success: function (data) {
			        console.log(data);
			        brands = data;
			    }
	        });

	        $("#entity").change(function() {
	        	selectedBrands = [];
	        	for (var i = 0; i < brands.length; i++) {
	        		if ($(this).val() == brands[i].entity_id) {
	        			selectedBrands.push(brands[i]);
	        		}
	        	}

	        	$("#brand").empty().append('<option value=""></option>');
	        	for (var i = 0; i < selectedBrands.length; i++) {
	        		$("#brand").append('<option value="' + selectedBrands[i].id + '">' + selectedBrands[i].description +'</option>');
	        	}

	        	if (selectedBrands.length == 1) {
	            	$("#brand")[0].selectedIndex = 1;
	            }

	            if ($("#entity option:selected").text() == 'ADMIN') {
	            	$("#billable").removeAttr('checked').attr('disabled', 'true');
	            } else {
	            	$("#billable").prop('checked', true).removeAttr('disabled');
	            }

	        });

		});

		
	</script>
@endsection