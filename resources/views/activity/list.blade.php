@extends('authenticated')

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well" action="{{ url('project') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="container-fluid">
			<div class="row">
				@if(isset($resources))
					<div class="form-group col-sm-3">
						<label for="projmngr">Ticket Manager:</label>
						<select name="projmngr" id="projmngr" class="form-control">
							<option value="">Select Ticket Manager</option>
							@foreach($resources as $projmngr)
								<?php $selected = ($projmngr->id == $defVal) ? "selected" : ""; ?>
								<option value="{{ $projmngr->id }}" $selected>{{ $projmngr->description }}</option>
							@endforeach
						</select>
					</div>
				@endif
				@if(isset($resources))
					<div class="form-group col-sm-3">
						<label for="description">Resource:</label>
						<select name="resource" id="resource" class="form-control">
							<option value="">Select Resource</option>
							@foreach($resources as $resource)
								<?php $selected = ($resource->id == $defVal) ? "selected" : ""; ?>
								<option value="{{ $resource->id }}">{{ $resource->description }}</option>
							@endforeach
						</select>
					</div>
				@endif
				@if(isset($resources) || isset($resources))
					<div class="col-xs-6 col-md-4 buttons">
						<label>&nbsp;</label>
						<button type="submit" name="search" class="btn btn-primary">
							<i class="fa fa-search-plus"></i> Search
						</button>
					</div>
				@endif
			</div>
			<br />
		</div>
	</form>
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3" style="margin-left: -10px"><h5><strong>List of Tickets</strong></h5></div>
					<div class="col-sm-3 col-sm-offset-6 right-align">
						<a href="project/add" class="btn btn-primary">
							<i class="fa fa-list-alt"></i> Add Ticket 
						</a>
					</div>
				</div>
			</div>		
		</div>
		<div class="panel-body">
			<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
				<table class="table table-striped table-bordered table-hover" id="dataTable">
					<tr>
						<th>#</th>
						<th>Ticket</th>
						<th>Status</th>
						<th>Start</th>
						<th>End</th>
						<th>Total Hours</th>
						<th>Action</th>
					</tr>
					<?php $i=1 ?>
					@foreach($projects as $project)
						<tr>
							<td>{{ $i++ }}.</td>
							<td>{{ $project->description }}</td>
							<td>{{ $project->status }}</td>
							<td>{{ $project->start_date }}</td>
							<td>{{ $project->end_date }}</td>
							{{-- <td>{{ $project->total_hours }}</td> --}}
							<td class="right-align">{{ number_format($project->end_date, 2) }}</td>
							<td>
								<a href="project/activity/{{ $project->id }}" class="btn btn-success" title="Log Activity"><i class="fa fa-clock-o"></i></a>
								<a href="project/details/{{ $project->id }}" class="btn btn-success" title="View Details"><i class="fa fa-search"></i></a>
								<a href="project/edit/{{ $project->id }}" class="btn btn-success" title="Edit"><i class="fa fa-edit"></i></a>
								<a href="project/del/{{ $project->id }}" class="btn btn-danger" title="Delete"><i class="fa fa-remove"></i></a>
							</td>
						</tr>

					@endforeach
				</table>
			</div>
		
		</div>
		
	</div>
@endsection