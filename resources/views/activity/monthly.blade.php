@extends('authenticated')
@section('additionalstylesheets')
<style type="text/css">
	/* calendar */
	table.calendar		{ border-left:1px solid #999; }
	tr.calendar-row	{  }
	td.calendar-day	{ min-height:80px; font-size:11px; position:relative; } * html div.calendar-day { height:80px; }
	td.calendar-day:hover	{ background:#eceff5; }
	td.calendar-day-np	{ background:#eee; min-height:80px; } * html div.calendar-day-np { height:80px; }
	td.calendar-day-head { background:#ccc; font-weight:bold; text-align:center; width:120px; padding:5px; border-bottom:1px solid #999; border-top:1px solid #999; border-right:1px solid #999; }
	div.day-number		{ background:#999; padding:5px; color:#fff; font-weight:bold; float:right; margin:-5px -5px 0 0; width:20px; text-align:center; }
	/* shared */
	td.calendar-day, td.calendar-day-np { width:120px; padding:5px; border-bottom:1px solid #999; border-right:1px solid #999; }
	.calendar-day p { text-align: center; font-size: 14px; font-weight: bold }

</style>

@endsection
@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<div class="panel panel-info">
		<div class="panel-heading">Calendar View</div>
		<div class="panel-body">
			<h3>{{ $monthName}} {{$year}} ({{ $resource->firstname }} {{$resource->lastname}})</h3>
			<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
				<table cellpadding="0" cellspacing="0" class="calendar table">
					<tr class="calendar-row">
						@foreach($days as $day)
							<td class="calendar-day-head">{{ $day }}</td>
						@endforeach
					</tr>
					<tr class="calendar-row">
						<?php $dayCounter = 0; ?>
						<?php $daysInThisWeek = 1; ?>
						@for($x = 0; $x < $runningDay; $x++)
							<td class="calendar-day-np"> </td>
							<?php $daysInThisWeek++; ?>
						@endfor

						@for($listDay = 1; $listDay <= $daysInMonth; $listDay++)
							<td class="calendar-day">
								{{-- add in the day number --}}
								<div class="day-number"> {{ $listDay }}</div>
								<p>{{ $data[$listDay] }}</p>
							</td>

							@if($runningDay == 6)
								</tr>
								@if(($dayCounter+1) != $daysInMonth)
									<tr class="calendar-row">
								@endif
								<?php $runningDay = -1; ?>
								<?php $daysInThisWeek = 0; ?>
							@endif
							<?php $daysInThisWeek++; $runningDay++; $dayCounter++; ?>
						@endfor

						@if($daysInThisWeek < 8)
							@for($x = 1; $x <= (8 - $daysInThisWeek); $x++)
								<td class="calendar-day-np"> </td>
							@endfor
						@endif

					</tr>
				</table>
				{{-- echo $calendar; --}}
			</div>
		
		</div>
		
	</div>
@endsection