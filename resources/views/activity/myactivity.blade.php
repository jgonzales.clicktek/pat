@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		{{-- <input type="hidden" name="project_id" value="{{ $project->id }}"> --}}
		<div class="container-fluid">
			<div class="row">
				@if(Auth::user()->is_admin)
					<div class="form-group col-sm-3">
						<label for="description">Resource:</label>
						<select name="resource" id="resource" class="form-control">
							<option value="">Select Resource</option>
							@foreach($resources as $resource)
								<option value="{{ $resource->id }}" @if($defaultResource == $resource->id) selected="selected" @endif>{{$resource->firstname}} {{$resource->lastname  }}</option>
							@endforeach
						</select>
					</div>
				@endif
				<div class="form-group col-sm-3">
					<label for="fromDate">Start:</label>
					<input type="text" name="fromDate" id="fromDate" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $fromDate }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="end">End:</label>
					<input type="text" name="toDate" id="toDate" class="form-control datepicker" placeholder="yyyy-mm-dd"  value="{{ $toDate }}">
				</div>
				<div class="col-sm-3 buttons">
					<label>&nbsp;</label>
					<button type="submit" name="search" class="btn btn-primary">
						<i class="fa fa-search-plus"></i> Search
					</button>
				</div>
				<br />
			</div>
			<br />
		</div>
	</form>

<div class="panel panel-info">
	<div class="panel-heading">
		My Activities
	</div>
	<div class="panel-body">
		<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
			<table class="table table-striped table-bordered table-hover" id="dataTable">
				<tr>
					<th>#</th>
					<th>Date Log</th>
					<th>Ticket</th>
					<th>Activity</th>
					<th>Entity</th>
					<th>Brand</th>
					<th>Status</th>
					<th>Hours</th>
					<th>Action</th>
				</tr>
				<?php $i=1 ?>
				<?php $totalHours = 0 ?>
				@foreach($activities as $activity)
					<tr>
						<td>{{ $i++ }}.</td>
						<td nowrap="">{{ $activity->date_log }}</td>
						<td><a href="{{ url('project/details')}}/{{$activity->project_id}}">{{ $activity->project }} </a> </td>
						<td>{{ $activity->activity }}</td>
						<td>{{ $activity->entity }}</td>
						<td>{{ $activity->brand }}</td>
						<td>{{ $activity->status }}</td>
						<td class="right-align">{{ number_format($activity->hours, 2) }}</td>
						<td nowrap="">
							<a href="{{ url('activity/edit')}}/{{$activity->id}}" class="btn btn-success" title="Edit"><i class="fa fa-edit"></i></a>
							<a href="{{ url('activity/del')}}/{{$activity->id}}" class="btn btn-danger" title="Delete"><i class="fa fa-remove"></i></a>
							<?php $totalHours += $activity->hours; ?>
						</td>
					</tr>
				@endforeach
				<tr>
					<td colspan="7" class="right-align">TOTAL HOURS </td>
					<td class="right-align">{{ number_format($totalHours, 2) }}</td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});
	</script>
@endsection