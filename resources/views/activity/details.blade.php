@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well" action="{{ url('project/update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="project_id" value="{{ $project->id }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="project">Ticket Name:</label>
					<input type="text" name="project" id="project" class="form-control" placeholder="Ticket Name" required="" value="{{ $project->description or '' }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="projmngr">Ticket Manager:</label>
					<select name="projmngr" id="projmngr" class="form-control">
						<option value="">Select Ticket Manager</option>
						@foreach($resources as $projmngr)
							<option value="{{ $projmngr->id }}" @if($project->project_manager_id == $projmngr->id) selected="selected" @endif>{{$projmngr->firstname}} {{$projmngr->lastname}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="entity">Entity:</label>
					<select name="entity" id="entity" class="form-control">
						<option value="">Select Entity</option>
						@foreach($entities as $entity)
							<option value="{{ $entity->id }}" @if($project->entity_id == $entity->id) selected="selected" @endif>{{ $entity->description }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="brand">Brand:</label>
					<select name="brand" id="brand" class="form-control">
						<option value="">Select Brand</option>
						@foreach($brands as $brand)
							<option value="{{ $brand->id }}" @if($project->brand_id == $brand->id) selected="selected" @endif>{{ $brand->description }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="start">Start:</label>
					<input type="start" name="start" id="start" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $project->start_date }}">
				</div>
			<!--/div>
			<div class="row"-->
				<div class="form-group col-sm-3">
					<label for="end">End:</label>
					<input type="end" name="end" id="end" class="form-control datepicker" placeholder="yyyy-mm-dd"  value="{{ $project->end_date }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="closed">Date Closed:</label>
					<input type="closed" name="closed" id="closed" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $project->closed_date }}">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="status">Status:</label>
					<select name="status" id="status" class="form-control">
						<option value="">Select Status</option>
						@foreach($statuses as $status)
							<option value="{{ $status }}" @if($project->status == $status) selected="selected" @endif>{{ $status }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="hours">Hours:</label>
					<input type="text" name="hours" id="hours" class="form-control" placeholder="Hours"  value="{{ $project->total_hours }}">
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-6 col-md-4 buttons">
					<button type="submit" name="action" class="btn btn-primary">
						<i class="fa fa-save"></i> Save
					</button>
				</div>
			</div>
		</div>
	</form>

<div class="panel panel-info">
	<div class="panel-heading">
		Activities
	</div>
	<div class="panel-body">
		<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
			<table class="table table-striped table-bordered table-hover" id="dataTable">
				<tr>
					<th>#</th>
					<th>Ticket</th>
					<th>Status</th>
					<th>Start</th>
					<th>End</th>
					<th>Total Hours</th>
					<th>Action</th>
				</tr>
				<?php $i=1 ?>
				@foreach($activities as $activity)
					<tr>
						<td>{{ $i++ }}.</td>
						<td>{{ $activity->date_log }}</td>
						<td>{{ $activity->type_id }}</td>
						<td>{{ $activity->activity }}</td>
						<td>{{ $activity->resource_id }}</td>
						{{-- <td>{{ $activity->hours }}</td> --}}
						<td class="right-align">{{ number_format($activity->hours, 2) }}</td>
						<td>
							<a href="project/activity/{{ $activity->id }}" class="btn btn-success" title="Log Activity"><i class="fa fa-clock-o"></i></a>
							<a href="project/details/{{ $activity->id }}" class="btn btn-success" title="View Details"><i class="fa fa-search"></i></a>
							<a href="project/edit/{{ $activity->id }}" class="btn btn-success" title="Edit"><i class="fa fa-edit"></i></a>
							<a href="project/del/{{ $activity->id }}" class="btn btn-danger" title="Delete"><i class="fa fa-remove"></i></a>
						</td>
					</tr>

				@endforeach
			</table>
		</div>
	</div>
</div>
@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});
	</script>
@endsection