@extends('authenticated')

@section('content')

<div class="panel panel-info">
	<div class="panel-heading">
		Edit Entity
	</div>
	<div class="panel-body">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form method="POST" class="form-inline well" action="{{url('entity')}}/{{$entity->id}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input name="_method" type="hidden" value="PATCH">
		<input name="id" type="hidden" value="{{ $entity->id }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="description">Description:</label>
					<input type="text" name="description" id="description" class="form-control" placeholder="Description" required="" value="{{ $entity->description }}">
				</div>
				<div class="form-group col-sm-8">
					<label></label>
					{{-- <br /> --}}
					<div class="checkbox form-group">
						<label><input type="checkbox" value="1" name="billable" @if($entity->billable) checked="" @endif> <b>Billable</b></label>
					</div>
				</div>
				<div class="form-group col-sm-8">
					<label></label>
					{{-- <br /> --}}
					<div class="checkbox form-group">
						<label><input type="checkbox" value="1" name="enabled" @if($entity->enabled) checked="" @endif> <b>Enabled</b></label>
					</div>
				</div>
			</div>
			<div class="row">
				<br />
				<div class="col-xs-6 col-md-4 buttons">
					<button type="submit" name="action" class="btn btn-primary">
						<i class="fa fa-save"></i> Save
					</button>
					<a type="button" href="{{ url('entity') }}" class="btn btn-default">
						<i class="fa fa-close"></i> Cancel
					</a>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
@endsection