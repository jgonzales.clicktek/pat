@extends('authenticated')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>
				<div class="panel-body">
					@if (isset($successmsg))
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							{{ $successmsg }}
						</div>
					@endif
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">Username</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">Resource</label>
							<div class="col-md-5">
								<select name="resource_id" id="resource_id" class="form-control">
									<option value="">Select Resource</option>
									@foreach($resources as $resource)
										<option value="{{ $resource->id }}" @if(old('resource_id') == $resource->id) selected="" @endif>{{$resource->firstname}} {{$resource->lastname  }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" style="padding-top: 0">User Type</label>
							<div class="col-md-5">
								<select name="usertype" class="form-control">
									<option value="0">Ordinary User</option>
									<option value="1">Admin User</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"></label>
							<div class="col-md-6">
								<span><i>Note:</i> The default password is 'password'</span>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
