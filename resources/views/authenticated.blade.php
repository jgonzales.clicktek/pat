<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Project Activity Tracking</title>
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/webtemplate/dist/css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset('/webtemplate/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap-select.min.css') }}" rel="stylesheet">
    @yield('additionalstylesheets')
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">Project Activity Tracking <sup>{{config('app.version')}} </sup></a>
                
            </div>
            <ul class="nav navbar-top-links navbar-right">
                @if(Auth::user()->is_admin)
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench fa-fw"></i>  
                            Maintenance
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{{ url('/entity') }}">Entities</a></li>
                            <li><a href="{{ url('/unit') }}">Business Unit</a></li>
                            <li><a href="{{ url('/brand') }}">Brands</a></li>
                            <li><a href="{{ url('/resource') }}">Resources</a></li>
                            <li><a href="{{ url('/status') }}">Statuses</a></li>
                            <li><a href="{{ url('/type') }}">Types</a></li>
                            <li><a href="{{ url('/settings') }}">Settings</a></li>
                            <li><a href="{{ url('/user/list') }}">Users</a></li>
                            <li><a href="{{ url('/services') }}">Services</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/user/register') }}">Register</a></li>
                @endif
                <li class="dropdown">
                    
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  
                        {{ Auth::user()->username }}
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="{{ url('/user/change-password') }}">Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="{{ url('/project') }}">Tickets</a></li>
                        <li><a href="{{ url('/my_projects') }}">My Tickets</a></li>
                        <li><a href="{{ url('/recent_projects') }}">My Recent Tickets</a></li>
                        <li><a href="{{ url('/my_activity') }}">My Activities</a></li>
                        <!-- <li><a href="{{ url('team_report') }}">Report</a></li> -->
                        <li>
                            <a href="#">
                                {{-- <i class="fa fa-wrench fa-fw"></i> --}}
                                Report
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="{{ url('/team_report') }}">Report by Ticket Owner</a></li>
                                <li><a href="{{ url('/report_by_business_unit') }}">Report by Business Unit</a></li>
                                <li><a href="{{ url('/report_by_resource_activity') }}">Report by Resource</a></li>
                                <li><a href="{{ url('/ticket_activity_log') }}">Ticket Activity Log</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('/summary') }}">Summary</a></li>
                        
                        @if(Auth::user()->is_admin)
                            <li><a href="{{ url('/sharing') }}">Entity Sharing</a></li>
                            <li><a href="#">
                                    {{-- <i class="fa fa-wrench fa-fw"></i> --}}
                                        Maintenance
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level collapse">
                                    <li><a href="{{ url('/entity') }}">Entities</a></li>
                                    <li><a href="{{ url('/unit') }}">Business Unit</a></li>
                                    <li><a href="{{ url('/brand') }}">Brands</a></li>
                                    <li><a href="{{ url('/resource') }}">Resources </a></li>
                                    <li><a href="{{ url('/status') }}">Statuses</a></li>
                                    <li><a href="{{ url('/type') }}">Types</a></li>
                                    <!-- <li><a href="{{ url('/settings') }}">Settings</a></li> -->
                                    <li><a href="{{ url('/user/list') }}">Users</a></li>
                                    <!-- <li><a href="{{ url('/services') }}">Services</a></li> -->
                                </ul>
                            </li>
                         @endif
					</ul>
				</div>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <!--h1 class="page-header"></h1-->
                </div>
            </div>

            @yield('content')

        </div>
    </div>

    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/webtemplate/bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ asset('/webtemplate/dist/js/sb-admin-2.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    @yield('customjs')
</body>

</html>