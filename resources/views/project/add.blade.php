@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="project">Ticket Name:</label>
					<input type="text" name="project" id="project" class="form-control" placeholder="Ticket Name" required="" value="{{ old('project') }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="projmngr">Ticket Manager:</label>
					<select name="projmngr" id="projmngr" class="form-control">
						<option value="">Select Ticket Manager</option>
						@foreach($resources as $projmngr)
							<option value="{{ $projmngr->id }}" @if(old('projmngr') == $projmngr->id) selected="selected" @endif>{{$projmngr->firstname}} {{$projmngr->lastname}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="ticket_type">Ticket Type:</label>
					<select name="ticket_type" id="ticket_type" class="form-control">
						<option value="">Select Ticket Type</option>
						@foreach($ticketTypes as $ticket)
							<option value="{{ $ticket->id }}" @if(old('ticket_type') == $ticket->id) selected="selected" @endif>{{$ticket->name}}</option>
						@endforeach
					</select>
				</div>
				{{-- <div class="form-group col-sm-3">
					<label for="entity">Entity:</label>
					<select name="entity" id="entity" class="form-control">
						<option value="">Select Entity</option>
						@foreach($entities as $entity)
							<option value="{{ $entity->id }}" @if(old('entity') == $entity->id) selected="selected" @endif>{{ $entity->description }}</option>
						@endforeach
					</select>
				</div> --}}
				<div class="form-group col-sm-3">
					<label for="status">Status:</label>
					<select name="status" id="status" class="form-control">
						<option value="">Select Status</option>
						@foreach($statuses as $status)
							<option value="{{ $status->id }}" $selected>{{ $status->description }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="start">Start:</label>
					<input type="start" name="start" id="start" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ old('start') }}" autocomplete="off">
				</div>
			<!--/div>
			<div class="row"-->
				<div class="form-group col-sm-3">
					<label for="end">End:</label>
					<input type="end" name="end" id="end" class="form-control datepicker" placeholder="yyyy-mm-dd"  value="{{ old('end') }}"  autocomplete="off">
				</div>
				<div class="form-group col-sm-3">
					<label for="closed">Date Closed:</label>
					<input type="closed" name="closed" id="closed" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ old('closed') }}"  autocomplete="off">
				</div>
				{{-- <div class="form-group col-sm-3">
					<label for="hours">Hours:</label>
					<input type="text" name="hours" id="hours" class="form-control" placeholder="Hours"  value="{{ old('hours') }}">
				</div> --}}
				<!-- <div class="form-group col-sm-3" id="estimate" style="display:none"> -->
				<div class="form-group col-sm-3" id="estimate">
					<label for="estimate">Estimate:</label>
					<input type="number" name="estimate" class="form-control" placeholder="Estimate"  value="{{ old('estimate') }}">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3" id="repository" >
					<label for="repository">Repository:</label>
					<input type="text" name="repository" class="form-control" placeholder="Repository"  value="{{ old('repository') }}">
				</div>
				<div class="form-group col-sm-3" id="repository" >
					<label for="remarks">Remarks:</label>
					<input type="text" name="remarks" class="form-control" placeholder="remarks"  value="{{ old('remarks') }}">
				</div>
			</div>
			<hr />
			<div class="row">
				{{-- <div class="col-md-4 col-md-offset-8 buttons" style="text-align: right"> --}}
				<div class="col-xs-6 col-md-4 buttons">
					<button type="submit" name="action" class="btn btn-primary">
						<i class="fa fa-save"></i> Save
					</button>
					<a type="button" href="{{ url('project') }}" class="btn btn-default">
						<i class="fa fa-close"></i> Cancel
					</a>
				</div>
			</div>
		</div>
	</form>

@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});
	</script>
	<!-- <script type="text/javascript">
		// $(document).ready(function() {
		// 	$('#estimate').css('display', 'none');
		// });
		$("#ticket_type").change(function () {
		var selected_option = $('#ticket_type').val();

			if (selected_option == 1) {
				$('#estimate').show();
			}else{
				$('#estimate').hide();
			}
		})
	</script> -->
@endsection