@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well" action="{{ url('project/update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="project_id" value="{{ $project->id }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="project">Ticket Name:</label>
					<input type="text" name="project" id="project" class="form-control" placeholder="Ticket Name" value="{{ $project->description or '' }}"  readonly="">
				</div>
				<div class="form-group col-sm-3">
					<label for="projRef">Ticket Ref #:</label>
					<input type="text" name="projRef" id="projRef" class="form-control" value="{{ $project->project_ref }}" readonly="">
				</div>
				<div class="form-group col-sm-3">
					<label for="projmngr">Ticket Manager:</label>
					<select name="projmngr" id="projmngr" class="form-control" readonly="">
						@foreach($resources as $projmngr)
							@if($project->project_manager_id == $projmngr->id)
								<option value="{{ $projmngr->id }}"  selected="selected">{{$projmngr->firstname}} {{$projmngr->lastname}}</option>
							@endif
						@endforeach
					</select>
				</div>
				{{-- <div class="form-group col-sm-3">
					<label for="entity">Entity:</label>
					<select name="entity" id="entity" class="form-control" readonly="">
						@foreach($entities as $entity)
							@if($project->entity_id == $entity->id) 
								<option value="{{ $entity->id }}" selected="selected">{{ $entity->description }}</option>
							@endif
						@endforeach
					</select>
				</div> --}}
				<div class="form-group col-sm-3">
					<label for="status">Status:</label>
					<select name="status" id="status" class="form-control" readonly="">
						@foreach($statuses as $status)
							@if($project->status_id == $status->id)
								<option value="{{ $status->id }}"  selected="selected">{{ $status->description }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="start">Start:</label>
					<input type="start" name="start" id="start" class="form-control" value="{{ $project->start_date }}" readonly="">
				</div>
			<!--/div>
			<div class="row"-->
				<div class="form-group col-sm-3">
					<label for="end">End:</label>
					<input type="end" name="end" id="end" class="form-control" value="{{ $project->end_date }}" readonly="">
				</div>
				<div class="form-group col-sm-3">
					<label for="closed">Date Closed:</label>
					<input type="closed" name="closed" id="closed" class="form-control" value="{{ $project->closed_date }}" readonly="">
				</div>
				<div class="form-group col-sm-3">
					<label for="hours">Total Hours:</label>
					<input type="text" name="hours" id="hours" class="form-control" value="{{ $project->total_hours }}" readonly="">
				</div>
				
			</div>
			<div class="row">
				@foreach($ticketTypes as $ticket)
					@if($ticket->id == 1)
						<div class="form-group col-sm-3">
							<label for="estimate">Estimate:</label>
							<input type="text" name="estimate" id="estimate" class="form-control" value="{{ $project->estimate_hours }}" readonly="">
						</div>
					@endif
				@endforeach
				<div class="form-group col-md-9">
					<label for="repository">Repository:</label>
					<a href="{{ $project->repository }}" target="_blank">{{ $project->repository }}</a>
				</div>
			</div>
			{{-- <hr>
			<div class="row">
				<div class="col-xs-6 col-md-4 buttons">
					<button type="submit" name="action" class="btn btn-primary">
						<i class="fa fa-save"></i> Save
					</button>
				</div>
			</div> --}}
		</div>
	</form>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete the selected activity?</p>
      </div>
      <div class="modal-footer">
      	<a href="" type="button" class="btn btn-danger" data-dismiss="modal" id="delete">Delete</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-info">
	<div class="panel-heading">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3" style="margin-left: -10px"><h5><strong>Activities</strong></h5></div>
				<div class="col-sm-3 col-sm-offset-6 right-align">
					<a href="{{ url('activity/add')}}/{{$project->id}}" class="btn btn-primary">
						<i class="fa fa-list-alt"></i> Add Activity 
					</a>
				</div>
			</div>
		</div>		
	</div>

	<div class="panel-body">
		<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
			<div class="container-fluid">  {{-- action="{{ url('project/details/filter') }}" --}}
				<form method="POST" class="form-inline ">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row">
					<div class="col-sm-3 ">
						<label for="projmngr">Filter Resource:</label>
						<select name="resource" id="resource" class="form-control">
							<option value="">Select Resource</option>
							@foreach($resources as $resource)
								<option value="{{ $resource->id }}" @if($resource->id == $defaultResource) selected="selected" @endif>{{$resource->firstname}} {{$resource->lastname}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 ">
						<label for="business_unit">Business Unit:</label>
						<select name="business_unit" id="business_unit" class="form-control">
							<option value="">All</option>
							@foreach($units as $unit)
								<option value="{{ $unit->id }}" @if($unit->id == $selectedUnit) selected="selected" @endif>{{$unit->description}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-1 buttons">
						<label>&nbsp;</label>
						<button type="submit" name="search" class="btn btn-primary">
							<i class="fa fa-search-plus"></i> Filter
						</button>
					</div>
					<div class="col-sm-5 text-right">
						<?php echo $activities->render(); ?>
					</div>
				</div>
				</form>
			</div>
			<table class="table table-striped table-bordered table-hover" id="dataTable">
				<tr>
					<th>#</th>
					<th>Date Log</th>
					<th>Activity</th>
					<th>Brand</th>
					<th>Type</th>
					<th>Resource</th>
					<th>Hours</th>
					<th>Action</th>
				</tr>
				<?php $i = (($activities->currentPage() - 1 ) * $activities->perPage() +1 ) ?>
				@foreach($activities as $activity)
					<tr>
						<td>{{ $i++ }}.</td>
						<td nowrap="">{{ $activity->date_log }}</td>
						<td>{{ $activity->activity }}</td>
						<td>{{ $activity->brand }}</td>
						<td>{{ $activity->act_type }}</td>
						<td>{{$activity->firstname}} {{$activity->lastname}}</td>
						<td class="right-align">{{ number_format($activity->hours, 2) }}</td>
						<td nowrap="">
						@if(Auth::user()->resource_id == $activity->resource_id)
							<div class="btn-group" role="group" style="display: flex;">
								<a href="{{ url('activity/edit') }}/{{ $activity->id }}" class="btn btn-default" title="Edit"><i class="fa fa-edit"></i></a>
								{{-- <a href="{{ url('activity/del') }}/{{ $activity->id }}" class="btn btn-danger" title="Delete"><i class="fa fa-remove"></i></a> --}}
								<button class="btn btn-danger btnDel" title="Delete" data-toggle="modal" data-target="#myModal" data-to-pass="{{ url('activity/del') }}/{{ $activity->id }}"><i class="fa fa-remove"></i></button>
							</div>
						@endif
						</td>
					</tr>

				@endforeach
			</table>
			<div class="col-sm-6 col-sm-offset-6 right-align">
				<?php echo $activities->render(); ?>
			</div>
			
		</div>
	</div>
</div>
@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});

		$(".btnDel").click(function() {
			var url = $(this).attr('data-to-pass');
			$(".modal-footer a").attr('href', url);
		});

		$("#delete").click(function() {
			var url = $(this).attr('href');
			window.location = url;
		});
	</script>
@endsection