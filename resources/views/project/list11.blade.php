@extends('authenticated')

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well" action="{{ url('project/search') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="container-fluid">
			<div class="row">
				@if(isset($resources))
					<div class="form-group col-sm-3">
						<label for="projmngr">Project Manager:</label>
						<select name="projmngr" id="projmngr" class="form-control">
							<option value="">Select Project Manager</option>
							@foreach($resources as $projmngr)
								<option value="{{ $projmngr->id }}" @if($defaultProjmngr == $projmngr->id) selected="selected" @endif>{{$projmngr->firstname}} {{$projmngr->lastname  }}</option>
							@endforeach
						</select>
					</div>
				@endif
				@if(isset($resources))
					<div class="form-group col-sm-3">
						<label for="description">Resource:</label>
						<select name="resource" id="resource" class="form-control">
							<option value="">Select Resource</option>
							@foreach($resources as $resource)
								<option value="{{ $resource->id }}" @if($defaultResource == $resource->id) selected="selected" @endif>{{$resource->firstname}} {{$resource->lastname  }}</option>
							@endforeach
						</select>
					</div>
				@endif
				@if(isset($resources))
					<div class="form-group col-sm-3">
						<label for="description">Status:</label>
						<select name="status" id="status" class="form-control">
							<option value="">Select Status</option>
							@foreach($statuses as $status)
								<option value="{{ $status->id }}" @if($defaultStatus == $status->id) selected="selected" @endif>{{$status->description}}</option>
							@endforeach
						</select>
					</div>
				@endif
				@if(isset($resources) || isset($resources))
					<div class="col-xs-6 col-sm-3 buttons">
						<label>&nbsp;</label>
						<button type="submit" name="search" class="btn btn-primary">
							<i class="fa fa-search-plus"></i> Search
						</button>
					</div>
				@endif
				
			</div>
			<br />
		</div>
	</form>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete the selected project and its activities?</p>
      </div>
      <div class="modal-footer">
      	<a href="" type="button" class="btn btn-danger" data-dismiss="modal" id="delete">Delete</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3" style="margin-left: -10px"><h5><strong>List of Projects</strong></h5></div>
					<div class="col-sm-3 col-sm-offset-6 right-align">
						<a href="{{ url('project/add')}}" class="btn btn-primary">
							<i class="fa fa-list-alt"></i> Add Project 
						</a>
					</div>
				</div>
			</div>		
		</div>
		<div class="panel-body">
			<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
				<table class="table table-striped table-bordered table-hover" id="dataTable">
					<tr>
						<th>#</th>
						<th>Project Ref#</th>
						<th>Project</th>
						<th>Status</th>
						<th>Start</th>
						<th>End</th>
						<th>Total Hours</th>
						<th>Action</th>
					</tr>
					<?php $i=1 ?>
					@foreach($projects as $project)
						<tr>
							<td>{{ $i++ }}.</td>
							<td>{{ $project->project_ref }}</td>
							<td>{{ $project->description }}</td>
							<td>{{ $project->status }}</td>
							<td>{{ $project->start_date }} </td>
							<td>{{ $project->end_date }}</td>
							<td class="right-align">{{ number_format($project->total_hours, 2) }}</td>
							<td nowrap="">
								<div class="btn-group" role="group" style="display: flex;">
									<a href="{{ url('activity/add')}}/{{ $project->id }}" class="btn btn-default" title="Log Activity"><i class="fa fa-clock-o"></i></a>
									<a href="{{ url('project/details')}}/{{ $project->id }}" class="btn btn-default" title="View Details"><i class="fa fa-search"></i></a>
									<a href="{{ url('project/edit') }}/{{ $project->id }}" class="btn btn-default" title="Edit"><i class="fa fa-edit"></i></a>
									@if(Auth::user()->is_admin || (Auth::user()->username==$project->added_by))
										<button class="btn btn-danger btnDel" title="Delete" data-toggle="modal" data-target="#myModal" data-to-pass="{{url('project/del') }}/{{ $project->id }}"><i class="fa fa-remove"></i></button>
									@endif
								</div>
							</td>
						</tr>

					@endforeach
				</table>
			</div>
		
		</div>

	</div>
@endsection
@section('customjs')
<script type="text/javascript">
	$(".btnDel").click(function() {
		var url = $(this).attr('data-to-pass');
		$(".modal-footer a").attr('href', url);
	});

	$("#delete").click(function() {
		var url = $(this).attr('href');
		window.location = url;
	});
</script>
@endsection