@extends('authenticated')

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well" action="{{ url('summary') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="container-fluid">
			<div class="row">
				@if(isset($years))
					<div class="form-group col-sm-3">
						<label for="year">Year:</label>
						<select name="year" id="year" class="form-control">
							@foreach($years as $dByear)
								<option value="{{ $dByear->year }}" @if($selectedYear == $dByear->year) selected="selected" @endif>{{$dByear->year }}</option>
							@endforeach
						</select>
					</div>
				@endif
				<div class="form-group col-sm-3">
					<label for="business_unit">Business Unit:</label>
					<select name="business_unit" id="business_unit" class="form-control">
						<option value="">All</option>
						@foreach($units as $unit)
							<option value="{{ $unit->id }}" @if(isset($filter['business_unit']) && $filter['business_unit'] == $unit->id) selected="selected" @endif >{{ $unit->description }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="resource">Resource Type:</label>
					<select name="resource" id="resource" class="form-control">
						<option value="-1">All</option>
						@foreach($resources as $key => $resource)
							<option value="{{ $key }}" @if($key == $selectedResource) selected="selected" @endif >{{$resource }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-2">
					{{-- <label>&nbsp;</label> --}}
					<br />
					<label><input type="checkbox" name="includeAdmin" @if ($includeAdmin) checked="" @endif> Include Admin</label>
				</div>
				
				<div class="col-xs-6 col-md-4 buttons">
					<label>&nbsp;</label>
					<button type="submit" name="search" class="btn btn-primary">
						<i class="fa fa-search-plus"></i> Search
					</button>
				</div>
			</div>
			<br />
		</div>
	</form>
	<div class="panel panel-info">
		<div class="panel-heading">Summary (manhour tally)</div>
		<div class="panel-body">
			<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
				<table class="table table-striped table-bordered table-hover" id="dataTable">
					<tr>
						<th>Name</th>
						@foreach($months as $month) 
							<th class="center">{{ $month }}</th>
						@endforeach
						<th>Total</th>
					</tr>
					@foreach($summary as $name => $val)
						<tr>
							<td>{{ $name }}</td>
							<?php $total = 0; ?>
							@for($i = 1; $i <= 12 ; $i++)
								<td class="center">
									@if(isset($val[$i]))
										<a href="{{url('summary/month')}}/{{$i}}/year/{{$selectedYear}}/resource/{{$summary[$name]['id']}}">
										{{ $val[$i] or '' }}
										</a>
										<?php $total += $val[$i]; ?>
									@endif
								</td>
							@endfor
							<td class="text-right">{{ $total }}</td>
						</tr>
					@endforeach
				</table>
			</div>
		
		</div>
		
	</div>
@endsection