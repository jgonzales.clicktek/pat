@extends('authenticated')

@section('additionalstylesheets')
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form method="POST" class="form-inline well" action="{{ url('project/update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="project_id" value="{{ $project->id }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="hours">Ticket Ref #:</label>
					<input type="text" name="hours" id="hours" class="form-control"  value="{{ $project->project_ref }}" readonly="">
				</div>
				<div class="form-group col-sm-3">
					<label for="project">Ticket Name:</label>
					<input type="text" name="project" id="project" class="form-control" placeholder="Ticket Name" required="" value="{{ $project->description or '' }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="projmngr">Ticket Manager:</label>
					<select name="projmngr" id="projmngr" class="form-control">
						<option value="">Select Ticket Manager</option>
						@foreach($resources as $projmngr)
							<option value="{{ $projmngr->id }}" @if($project->project_manager_id == $projmngr->id) selected="selected" @endif>{{$projmngr->firstname}} {{$projmngr->lastname}}</option>
						@endforeach
					</select>
				</div>
				{{-- <div class="form-group col-sm-3">
					<label for="entity">Entity:</label>

					<select name="entity" id="entity" class="form-control" @if($project->total_hours > 0 ) disabled="" @endif>
						<option value="">Select Entity</option>
						@foreach($entities as $entity)
							<option value="{{ $entity->id }}" @if($project->entity_id == $entity->id) selected="selected" @endif>{{ $entity->description }}</option>
						@endforeach
					</select>
				</div> --}}
				<div class="form-group col-sm-3">
					<label for="status">Status:</label>
					<select name="status" id="status" class="form-control">
						<!-- <option value="">Select Status</option> -->
						@foreach($statuses as $status)
							<option value="{{ $status->id }}" @if($project->status_id == $status->id) selected="selected" @endif>{{ $status->description }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="start">Start:</label>
					<input type="start" name="start" id="start" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $project->start_date }}" autocomplete="off">
				</div>
			<!--/div>
			<div class="row"-->
				<div class="form-group col-sm-3">
					<label for="end">End:</label>
					<input type="end" name="end" id="end" class="form-control datepicker" placeholder="yyyy-mm-dd"  value="{{ $project->end_date }}" autocomplete="off">
				</div>
				<div class="form-group col-sm-3">
					<label for="closed">Date Closed:</label>
					<input type="closed" name="closed" id="closed" class="form-control datepicker" placeholder="yyyy-mm-dd" value="{{ $project->closed_date }}" autocomplete="off">
				</div>
				<div class="form-group col-sm-3">
					<label for="hours">Hours:</label>
					<input type="text" name="hours" id="hours" class="form-control" value="{{ $project->total_hours }}" readonly="">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="ticket_type_id">Ticket Type:</label>
					<select name="ticket_type" id="ticket_type" class="form-control">
						<!-- <option value="">Select Ticket Type</option> -->
						@foreach($ticketTypes as $ticket)
							<option value="{{ $ticket->id }}" @if($project->ticket_type_id == $ticket->id) selected="selected" @endif>{{ $ticket->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="estimate">Estimate:</label>
					<input type="number" name="estimate" id="estimate" class="form-control" value="{{ $project->estimate_hours }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="repository">Repository:</label>
					<input type="text" name="repository" id="repository" class="form-control" value="{{ $project->repository }}">
				</div>
				<div class="form-group col-sm-3">
					<label for="remarks">Remarks:</label>
					<input type="text" name="remarks" id="remarks" class="form-control" value="{{ $project->remarks }}">
				</div>
			</div>
				
			</div>

			<hr />
			<div class="row">
				<div class="col-xs-6 col-md-4 buttons">
					<button type="submit" name="action" class="btn btn-primary">
						<i class="fa fa-save"></i> Save
					</button>
					<a type="button" href="{{ url($cancelUrl) }}" class="btn btn-default">
						<i class="fa fa-close"></i> Cancel
					</a>
				</div>
			</div>
		</div>
	</form>
@endsection
@section('customjs')
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'});
	</script>
@endsection