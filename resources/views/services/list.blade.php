@extends('authenticated')

@section('content')

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete the selected services?</p>
				</div>
				<div class="modal-footer">
					<a href="" type="button" class="btn btn-danger" data-dismiss="modal" id="delete">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>

	</div>
</div>

<div class="panel panel-info">
	<div class="panel-heading">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3" style="margin-left: -10px"><h5><strong>List of Services</strong></h5></div>
					<div class="col-sm-3 col-sm-offset-6 right-align">
						<a href="{{ url('services/create') }}" class="btn btn-primary">
							<i class="fa fa-list-alt"></i> Add Services
						</a>
					</div>
				</div>
			</div>		
		</div>
	<div class="panel-body">
		@if (isset($successmsg))
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				{{ $successmsg }}
			</div>
		@endif
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form method="POST" class="form-inline well" action="{{ url('services/filter') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="container-fluid">
				<div class="row">
					<div class="form-group col-sm-3">
						<label for="enabled">Enabled:</label>
						<select name="enabled" id="enabled" class="form-control">
							<option value="">[ All ]</option>
							@foreach($options as $key => $val)
								<option value="{{ $key }}" @if(isset($filter['enabled']) && $filter['enabled'] == $key) selected="selected" @endif>{{$val}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-3">
						<label for="billable">Billable:</label>
						<select name="billable" id="billable" class="form-control">
							<option value="">[ All ]</option>
							@foreach($options as $key => $val)
								<option value="{{ $key }}" @if(isset($filter['billable']) && $filter['billable'] == $key) selected="selected" @endif>{{$val}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-6 col-md-4 buttons">
						<label>&nbsp;</label>
						<button type="submit" name="search" class="btn btn-primary">
							<i class="fa fa-search-plus"></i> Filter
						</button>
					</div>
				</div>
				<br />
			</div>
		</form>
		<div class="employerecords dataTable_wrapper table-responsive" id="tablelist">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example">
				<tr>
					<th>#</th>
					<th>Description</th>
					<th>Enabled</th>
					<th>Billable</th>
					<th>Action</th>
				</tr>
				<?php $i=1 ?>
				@foreach($services as $service)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{ $service->description }}</td>
					<td>@if($service->enabled == 1) Yes @else No @endif </td>
					<td>@if($service->billable == 1) Yes @else No @endif </td>
					<td>
						<a class="btn btn-primary fa fa-edit" href="{{ url('services') }}/{{$service->id}}/edit" title="Edit">
						</a>
						<button class="btn btn-danger btnDel fa fa-remove" title="Delete" data-toggle="modal" data-target="#myModal" data-to-pass="{{url('services/del') }}/{{ $service->id }}"></button>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
	<div class="panel-footer">
	</div>
</div>

@endsection
@section('customjs')
<script type="text/javascript">
	$(".btnDel").click(function() {
		var url = $(this).attr('data-to-pass');
		$(".modal-footer a").attr('href', url);
	});

	$("#delete").click(function() {
		var url = $(this).attr('href');
		window.location = url;
	});
</script>
@endsection