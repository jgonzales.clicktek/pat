@extends('authenticated')

@section('content')

<div class="panel panel-info">
	<div class="panel-heading">
		Edit Status
	</div>
	<div class="panel-body">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form method="POST" class="form-inline well" action="{{url('settings')}}/{{$settings->id}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input name="_method" type="hidden" value="PATCH">
		<input name="id" type="hidden" value="{{ $settings->id }}">
		<div class="container-fluid">
			<div class="row">
				<div class="form-group col-sm-3">
						<label for="entity_sharing">Entity Sharing:</label>
						<select name="entity_sharing" id="entity_sharing" class="form-control">
							@foreach($options as $key => $val)
								<option value="{{ $key }}" @if($settings->entity_sharing_old == $key) selected="selected" @endif>{{$val}}</option>
							@endforeach
						</select>
					</div>
			</div>
			<div class="row">
				<br />
				<div class="col-xs-6 col-md-4 buttons">
					<button type="submit" name="action" class="btn btn-primary">
						<i class="fa fa-save"></i> Save
					</button>
					<a type="button" href="{{ url('settings') }}" class="btn btn-default">
						<i class="fa fa-close"></i> Cancel
					</a>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
@endsection